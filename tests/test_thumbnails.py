# Tagcloud
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Uncomment for debug logs!
import logging, sys
logging.basicConfig(level=logging.DEBUG, stream=sys.stderr)


import gi
gi.require_version('GnomeDesktop', '3.0')
from gi.repository import GnomeDesktop

import pytest

import urllib.request
import warnings

import tagcloud.thumbnails
import testutils.generate
import testutils.mainloop


def test_thumbnail_mimetypes():
    '''Test that the Flatpak app environment has the thumbnailers we expect.'''

    expected_mime_types = [
        'image/jpeg',
        'image/png',
    ]

    factory = GnomeDesktop.DesktopThumbnailFactory.new(GnomeDesktop.DesktopThumbnailSize.NORMAL)

    for mime in expected_mime_types:
        assert factory.can_thumbnail('/tmp/ignored', mime, 0)


def test_jpeg_thumbnail(tmpdir):
    image = str(tmpdir.join('thumbnail-me.jpg'))
    testutils.generate.generate_jpeg(image)

    thread = tagcloud.thumbnails._ThumbnailerThread()
    thread.setDaemon(True)
    thread.start()

    loop = testutils.mainloop.MainLoop()
    def callback(pixbuf):
        assert pixbuf is not None, "Thumbnailing failed for {}".format(image)
        loop.quit()
    thread.create_thumbnail_for_url('file://' + urllib.request.pathname2url(image), callback)
    loop.run_checked(timeout=5)
