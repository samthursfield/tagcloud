# Tagcloud
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gi
from gi.repository import Gio, GLib, GObject

import pytest

import logging

import tagcloud.app
import tagcloud.locationmanager
import testutils.mainloop


def await_property_changed(obj, property_name):
    '''Run the main loop until the 'notify' signal for property_name is emitted.

    Note that any underscores in property_name should be converted to hyphens, see:
    https://gitlab.gnome.org/GNOME/pygobject/issues/228

    '''
    loop = testutils.mainloop.MainLoop()

    def property_changed(notify_obj, pspec):
        assert obj == notify_obj
        loop.quit()

    handler_id = obj.connect('notify::{}'.format(property_name), property_changed)
    loop.run_checked()
    GObject.signal_handler_disconnect(obj, handler_id)
    return obj.get_property(property_name)


def await_notifications_changed(app):
    return await_property_changed(app._notification_manager, 'notifications')


def await_locations_changed(app):
    return await_property_changed(app._location_manager, 'locations')


def await_removable_devices_changed(app):
    return await_property_changed(app._location_manager, 'removable-devices')


def test_notify(app):
    '''Test that we notify when removable devices appear and disappear.'''
    mock_volume_monitor = app.volume_monitor

    mock_device_1 = mock_volume_monitor.add_mock_mount('test1', '/tmp/test1/')
    notifications = await_notifications_changed(app)

    assert len(notifications) == 1
    assert notifications[0].device.mount == mock_device_1

    mock_device_2 = mock_volume_monitor.add_mock_mount('test2', '/tmp/test2/')
    notifications = await_notifications_changed(app)

    assert len(notifications) == 2
    assert notifications[0].device.mount == mock_device_1
    assert notifications[1].device.mount == mock_device_2

    # When a device is removed, the notification should go too...

    mock_volume_monitor.remove_mock_mount(mock_device_1)
    notifications = await_notifications_changed(app)

    assert len(notifications) == 1
    assert notifications[0].device.mount == mock_device_2

    # When user clicks on one of the buttons, we are done.
    notifications[0].ignore()

    notifications = await_notifications_changed(app)
    assert len(notifications) == 0


def test_actions_control_indexing(app, tmpdir):
    '''Test that removable device actions have the expected effect.'''
    mock_volume_monitor = app.volume_monitor

    mock_device_dir = str(tmpdir.mkdir('removable-device'))

    configured_locations = app._location_manager.locations

    # Connect a removable device.
    mock_device_1 = mock_volume_monitor.add_mock_mount('test1', mock_device_dir)

    # The device should now be known to the location manager...
    devices = await_removable_devices_changed(app)
    assert len(devices) == 1
    device = devices[0]
    assert device.uuid() == mock_device_1.get_uuid()

    # ...but it shouldn't yet appear as a location that's being indexed.
    assert device.get_action() == tagcloud.locationmanager.RemovableDeviceAction.ASK
    assert app._location_manager.locations == configured_locations

    # Now set the device for indexing: it should appear in locations
    device.set_action(tagcloud.locationmanager.RemovableDeviceAction.INDEX)
    locations = await_locations_changed(app)
    assert locations == sorted(configured_locations + [device])

    # Set it to be ignored: it should disappear from locations
    device.set_action(tagcloud.locationmanager.RemovableDeviceAction.IGNORE)
    locations = await_locations_changed(app)
    assert locations == configured_locations


def test_actions_persist(app, tmpdir):
    '''Test that removable device actions are saved between sessions.'''
    mock_volume_monitor = app.volume_monitor

    mock_device_dir = str(tmpdir.mkdir('removable-device'))
    mock_device_uuid = '1234AAAB'

    # The UUID of each mock device is the same, so the setting should be shared
    # as if they are all the same device.

    # First we set the action INDEX...
    mock_device_1 = mock_volume_monitor.add_mock_mount('test1', mock_device_dir, uuid=mock_device_uuid)
    device = await_removable_devices_changed(app)[0]

    device.set_action(tagcloud.locationmanager.RemovableDeviceAction.INDEX)

    mock_volume_monitor.remove_mock_mount(mock_device_1)

    # Check that worked and set it to IGNORE...
    mock_device_2 = mock_volume_monitor.add_mock_mount('test2', mock_device_dir, uuid=mock_device_uuid)
    device = await_removable_devices_changed(app)[0]

    assert device.get_action() == tagcloud.locationmanager.RemovableDeviceAction.INDEX
    device.set_action(tagcloud.locationmanager.RemovableDeviceAction.IGNORE)

    mock_volume_monitor.remove_mock_mount(mock_device_2)

    # Check that it's now IGNORE.
    mock_device_3 = mock_volume_monitor.add_mock_mount('test3', mock_device_dir, uuid=mock_device_uuid)
    devices = await_removable_devices_changed(app)

    assert len(devices) == 1
    device = devices[0]

    assert device.get_action() == tagcloud.locationmanager.RemovableDeviceAction.IGNORE

    mock_volume_monitor.remove_mock_mount(mock_device_3)
