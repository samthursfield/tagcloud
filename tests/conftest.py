# Tagcloud
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gio, GObject

import pytest
import trackerappdomain

import os

import tagcloud.app
import testutils.await
import testutils.mainloop
import testutils.volumemonitor


# Uncomment for debug logs!
import logging, sys
logging.basicConfig(level=logging.DEBUG, stream=sys.stderr)

# Set to 3 for lots of debug logs! You'll also need to set the 'logging'
# module to write to stdout.
TRACKER_DEBUG_VERBOSITY = 0


@pytest.fixture
def tracker_domain(tmpdir):
    '''Provides a throwaway TrackerAppDomain instance.'''
    domain_name = 'uk.me.afuera.Tagcloud.Test'
    domain_dir = tmpdir.ensure('tracker-domain', dir=True)
    with trackerappdomain.tracker_app_domain(domain_name, str(domain_dir), debug_verbosity=TRACKER_DEBUG_VERBOSITY) as tracker:
        yield tracker


__counter = 1
@pytest.fixture
def app(tmpdir):
    '''Provides an instance of the main Application class.'''
    global __counter

    resource = Gio.resource_load(os.path.join(os.environ['TAGCLOUD_RESOURCE_PATH'], 'tagcloud.gresource'))
    Gio.Resource._register(resource)

    mock_volume_monitor = testutils.volumemonitor.MockVolumeMonitor()

    # We create a domain but we don't start it, the app will take care of that.
    domain_name = 'uk.me.afuera.Tagcloud.Test'
    domain_dir = tmpdir.ensure('tracker-domain', dir=True)
    tracker_domain = trackerappdomain.TrackerAppDomain(domain_name, str(domain_dir), debug_verbosity=TRACKER_DEBUG_VERBOSITY)

    app = tagcloud.app.Application(tracker_domain,
                                   volume_monitor=mock_volume_monitor,
                                   app_id_suffix='.Test{}'.format(__counter),
                                   app_version='test')
    app.register()
    app.activate()

    __counter += 1

    testutils.await.await_signal(app, 'database_ready')

    yield app
