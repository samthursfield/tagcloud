# Tagcloud
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import GObject

import testutils.mainloop


def await_signal(obj, signal_name, timeout=10):
    '''Run the GLib main loop until an object emits the given signal.'''
    loop = testutils.mainloop.MainLoop()

    handler_id = obj.connect(signal_name, lambda obj: loop.quit())
    loop.run_checked(timeout=timeout)

    GObject.signal_handler_disconnect(obj, handler_id)
