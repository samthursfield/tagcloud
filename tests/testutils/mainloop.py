# Tagcloud
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gi
from gi.repository import GLib, GObject

import logging
import sys


G_SOURCE_REMOVE = False
G_SOURCE_CONTINUE = True


class MainLoop():
    '''Wrapper for GLib.MainLoop that propagates any unhandled exceptions.

    PyGObject doesn't seem to provide any help with propagating exceptions from
    the GLib main loop to the main Python execution context. The default
    behaviour is to print a message and continue, which is useless for tests as
    it means tests appear to pass when in fact they are broken.

    This class allows any such exceptions to be reported back to Pytest.

    '''

    def __init__(self):
        self._loop = GLib.MainLoop.new(None, 0)

    def quit(self):
        self._loop.quit()

    def run_checked(self, timeout=10):
        '''Run the loop until quit(), then raise any unhandled exception.

        If 'timeout' is an integer, it gives the maximum runtime of the loop in
        seconds. If the loop hasn't exited when the timeout is up, an exception
        will raised.

        '''
        self._exception = None

        old_hook = sys.excepthook

        def new_hook(etype, evalue, etb):
            logging.debug("Uncaught exception {}; exiting GLib main loop".format(etype))
            self._loop.quit()
            self._exception = evalue
            old_hook(etype, evalue, etb)

        def timeout_cb():
            self._loop.quit()
            self._exception = RuntimeError("Timed out after {} seconds".format(timeout))
            return G_SOURCE_CONTINUE

        if timeout:
            timeout_source_id = GLib.timeout_add_seconds(timeout, timeout_cb)
        else:
            timeout_source_id = None

        try:
            sys.excepthook = new_hook
            self._loop.run()
        finally:
            if timeout_source_id:
                GLib.source_remove(timeout_source_id)
            sys.excepthook = old_hook

        if self._exception:
            raise self._exception
