# Tagcloud
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gi
from gi.repository import Gio, GLib, GObject

import logging
import sys


class MockDrive(GObject.Object, Gio.Drive):
    def __init__(self, is_removable):
        super(MockDrive, self).__init__()
        self._is_removable = is_removable

    def do_changed(self, *args):
        pass

    def do_disconnected(self, *args):
        pass

    def do_eject_button(self, *args):
        pass

    def do_get_name(self, *args):
        pass

    def do_get_icon(self, *args):
        pass

    def do_has_volumes(self, *args):
        pass

    def do_get_volumes(self, *args):
        pass

    def do_is_media_removable(self, *args):
        pass

    def do_has_media(self, *args):
        pass

    def do_is_media_check_automatic(self, *args):
        pass

    def do_can_eject(self, *args):
        pass

    def do_can_poll_for_media(self, *args):
        pass

    def do_eject(self, *args):
        pass

    def do_eject_finish(self, *args):
        pass

    def do_poll_for_media(self, *args):
        pass

    def do_poll_for_media_finish(self, *args):
        pass

    def do_get_identifier(self, *args):
        pass

    def do_enumerate_identifiers(self, *args):
        pass

    def do_get_start_stop_type(self, *args):
        pass

    def do_can_start(self, *args):
        pass

    def do_can_start_degraded(self, *args):
        pass

    def do_start(self, *args):
        pass

    def do_start_finish(self, *args):
        pass

    def do_can_stop(self, *args):
        pass

    def do_stop(self, *args):
        pass

    def do_stop_finish(self, *args):
        pass

    def do_stop_button(self, *args):
        pass

    def do_eject_with_operation(self, *args):
        pass

    def do_eject_with_operation_finish(self, *args):
        pass

    def do_get_sort_key(self, *args):
        pass

    def do_get_symbolic_icon(self, *args):
        pass

    def do_is_removable(self, *args):
        return self._is_removable


class MockVolume(GObject.Object, Gio.Volume):
    def __init__(self):
        super(MockVolume, self).__init__()

    def do_changed(self, *args):
        pass

    def do_removed(self, *args):
        pass

    def do_get_name(self, *args):
        pass

    def do_get_icon(self, *args):
        pass

    def do_get_uuid(self, *args):
        pass

    def do_get_drive(self, *args):
        pass

    def do_get_mount(self, *args):
        pass

    def do_can_mount(self, *args):
        pass

    def do_can_eject(self, *args):
        pass

    def do_mount_fn(self, *args):
        pass

    def do_mount_finish(self, *args):
        pass

    def do_eject(self, *args):
        pass

    def do_eject_finish(self, *args):
        pass

    def do_get_identifier(self, *args):
        pass

    def do_enumerate_identifiers(self, *args):
        pass

    def do_should_automount(self, *args):
        pass

    def do_get_activation_root(self, *args):
        pass

    def do_eject_with_operation(self, *args):
        pass

    def do_eject_with_operation_finish(self, *args):
        pass

    def do_get_sort_key(self, *args):
        pass

    def do_get_symbolic_icon(self, *args):
        pass


class MockMount(GObject.Object, Gio.Mount):
    def __init__(self, name, path, is_removable=True, uuid=None):
        super(MockMount, self).__init__()
        self._name = name
        self._root = Gio.File.new_for_path(path)
        self._uuid = uuid
        self._drive = MockDrive(is_removable)
        self._volume = MockVolume()

    def do_changed(self, *args):
        pass

    def do_unmounted(self, *args):
        pass

    def do_get_name(self):
        return self._name

    def do_get_root(self, *args):
        return self._root

    def do_get_icon(self, *args):
        pass

    def do_get_uuid(self, *args):
        return self._uuid

    def do_get_volume(self, *args):
        return self._volume

    def do_get_drive(self, *args):
        return self._drive

    def do_can_unmount(self, *args):
        pass

    def do_can_eject(self, *args):
        pass

    def do_unmount(self, *args):
        pass

    def do_unmount_finish(self, *args):
        pass

    def do_eject(self, *args):
        pass

    def do_eject_finish(self, *args):
        pass

    def do_remount(self, *args):
        pass

    def do_remount_finish(self, *args):
        pass

    def do_guess_content_type(self, *args):
        pass

    def do_guess_content_type_finish(self, *args):
        pass

    def do_guess_content_type_sync(self, *args):
        pass

    def do_pre_unmount(self, *args):
        pass

    def do_unmount_with_operation(self, *args):
        pass

    def do_unmount_with_operation_finish(self, *args):
        pass

    def do_eject_with_operation(self, *args):
        pass

    def do_eject_with_operation_finish(self, *args):
        pass

    def do_get_default_location(self, *args):
        pass

    def do_get_sort_key(self, *args):
        pass

    def do_get_symbolic_icon(self, *args):
        pass


class MockVolumeMonitor(GObject.Object):
    '''A replacement for GVolumeMonitor for use when testing.

    It's possible to extend GVolumeMonitor by implementing the corresponding
    Gio extension point, but we can't /de/register the existing volume monitors
    which could lead to the tests becoming confused by real devices on the
    host. So instead we use this mock.

    '''
    def __init__(self):
        super(MockVolumeMonitor, self).__init__()
        self._mounts = []

    @GObject.Signal(arg_types=(GObject.Object,))
    def mount_added(self, mount):
        pass

    @GObject.Signal(arg_types=(GObject.Object,))
    def mount_removed(self, mount):
        pass

    def get_mounts(self):
        return self._mounts

    def add_mock_mount(self, name, path, uuid=None):
        mount = MockMount(name, path, uuid=uuid)
        self._mounts.append(mount)

        # This must be done in an idle so that tests are able to trigger the
        # logic, then connect to the signal they want to watch and run the main
        # loop until it's emitted.
        GLib.idle_add(lambda: self.emit('mount-added', mount))

        return mount

    def remove_mock_mount(self, mount):
        self._mounts.remove(mount)
        GLib.idle_add(lambda: self.emit('mount-removed', mount))
