# Tagcloud
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf, GLib, GObject

import pytest
import trackerappdomain

import tagcloud.contentlistmodel
import tagcloud.locationmanager
import testutils.generate
import testutils.mainloop


def await_items_changed(loop, model, timeout=10, position=None, removed=None, added=None):
    class TimeoutHolder():
        def __init__(self):
            self.timedout = False
    timeout_holder = TimeoutHolder()

    def items_changed_callback(model, _position, _removed, _added):
        loop.quit()
        assert _position == position
        assert _removed == removed
        assert _added == added

    def timeout_callback(holder):
        holder.timedout = True
        loop.quit()
    items_changed_handler = model.connect('items-changed', items_changed_callback)
    timeout_handler = GLib.timeout_add_seconds(timeout, timeout_callback, timeout_holder)
    loop.run_checked()
    GObject.signal_handler_disconnect(model, items_changed_handler)
    if timeout_holder.timedout:
        raise AssertionError("Timeout of {} seconds exceeded waiting for item to be added.".format(timeout))
    else:
        GLib.source_remove(timeout_handler)


def test_filesystem_monitoring(tmpdir, tracker_domain):
    '''Tests that changes on disk are detected by the ContentListModel.'''
    loop = testutils.mainloop.MainLoop()

    # Set up content directory with a single .jpeg image.
    content_dir = tmpdir.ensure('content', dir=True)
    testutils.generate.generate_jpeg(str(content_dir.join('image1.jpeg')))
    tracker_domain.index_location_async(str(content_dir), None, lambda: loop.quit())
    loop.run_checked()

    # Create a model and assert that it is populated with that iamge.
    model = tagcloud.contentlistmodel.ContentListModel(tracker_domain.connection())

    location = tagcloud.locationmanager.Location(str(content_dir))
    model.set_location_filter([location])
    await_items_changed(loop, model, position=0, removed=0, added=1)

    assert model.get_n_items() == 1
    # We must avoid using the g_list_model_get_item() method because of
    # https://bugzilla.gnome.org/show_bug.cgi?id=787271.
    assert model._view.items[0].file_name == 'image1.jpeg'

    # Create a second image on disk. Tracker should spot the new file and
    # notify the contentlistmodel, which should then add the item.
    testutils.generate.generate_jpeg(str(content_dir.join('image2.jpeg')))
    await_items_changed(loop, model, position=1, removed=0, added=1)
    assert model.get_n_items() == 2
    assert model._view.items[1].file_name == 'image2.jpeg'

    # Delete the first image and assert that it is removed.
    content_dir.join('image1.jpeg').remove()
    await_items_changed(loop, model, position=0, removed=1, added=0)
    assert model.get_n_items() == 1
    assert model._view.items[0].file_name == 'image2.jpeg'
