#!/bin/sh

# Taken from https://gitlab.com/BuildStream/buildstream-examples/

set -e

OUTPUT_DIR="./tagcloud-flatpak"
APP="uk.me.afuera.Tagcloud"
REF="app/${APP}/x86_64/master"
REMOTE="local-tagcloud"
FLATPAK="flatpak --user"

if [ -e "${OUTPUT_DIR}" ]; then
    echo >&2 "ERROR: Output directory ${OUTPUT_DIR} already exists."
    exit 1
fi

mkdir -p "${OUTPUT_DIR}/checkout"
mkdir -p "${OUTPUT_DIR}/checkout-debug"

echo "Checking out elements"
bst checkout --hardlinks "flatpak-${APP}.bst" "${OUTPUT_DIR}/checkout"
bst checkout --hardlinks "flatpak-${APP}.Debug.bst" "${OUTPUT_DIR}/checkout-debug"

# This is needed in order for the Debug extension to work correctly. BuildStream
# can't create this directory as its split-rules logic can't currently handle
# the case where a directory should go into the 'runtime' domain while all the
# files within the directory should go into the 'debug' domain.
mkdir -p "${OUTPUT_DIR}/checkout/files/lib/debug"

# This stuff is handled automatically by flatpak-builder, but not yet by BuildStream.
mkdir -p "${OUTPUT_DIR}/checkout/export/share/applications"
cp "${OUTPUT_DIR}/checkout/files/share/applications/uk.me.afuera.Tagcloud.desktop" "${OUTPUT_DIR}/checkout/export/share/applications"
mkdir -p "${OUTPUT_DIR}/checkout/export/share/icons/hicolor/scalable/apps"
cp "${OUTPUT_DIR}/checkout/files/share/icons/hicolor/scalable/apps/uk.me.afuera.Tagcloud.svg" "${OUTPUT_DIR}/checkout/export/share/icons/hicolor/scalable/apps"

echo "Exporting to OSTree repository"
flatpak build-export ${OUTPUT_DIR}/repo ${OUTPUT_DIR}/checkout master
flatpak build-export ${OUTPUT_DIR}/repo ${OUTPUT_DIR}/checkout-debug master

echo "Installing to Flatpak"
url="file://$(pwd)/${OUTPUT_DIR}/repo"
if ${FLATPAK} remotes --show-disabled | grep -q ${REMOTE}; then
    ${FLATPAK} remote-modify ${REMOTE} --no-gpg-verify --enable --url ${url}
else
    ${FLATPAK} remote-add --no-gpg-verify ${REMOTE} ${url}
fi
${FLATPAK} install -y --reinstall ${REMOTE} ${APP}
${FLATPAK} install -y --reinstall ${REMOTE} ${APP}.Debug
