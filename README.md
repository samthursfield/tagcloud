TAGCLOUD
========

Tagcloud is a GTK+ application that lets you explore all the junk on your
computer, and you can apply text labels to things to try and organise it.

It is at an early stage of development.

Here is a screenshot!

![](docs/screenshot-20170929.png)

Developer information
---------------------

Tagcloud requires GTK+ 3, Python 3, PyGObject and Tracker.

There is a BuildStream project provided which allows you to produce a Flatpak
application bundle of Tagcloud. The following commands should work assuming you
have BuildStream, the bst-external plugins repo and Flatpak available:

    cd flatpak
    bst build tagcloud-flatpak.bst
    ./deploy.sh

You can then run it with this command:

    flatpak run uk.me.afuera.Tagcloud

Tagcloud has an automated test suite which can be run via `meson test`.
The most reliable way to run this is inside the application bundle environment
using `bst shell`.

First you must build the test environment, and create a build tree with the
correct configuration, as in the following example:

    bst build tagcloud-test-environment.bst
    bst shell tagcloud-test-environment.bst --mount ~/tagcloud /src \
        -- sh -c 'cd /src; mkdir build; meson ./build --prefix=/app'

Once this is done, you can run the test suite as follows.

    bst shell tagcloud-test-environment.bst --mount ~/tagcloud /src \
        -- sh -c 'cd /src/build; xvfb-run dbus-launch meson test --print-errorlogs'

You can also use the test environment to run the Tagcloud application. This is
NOT the same as running it under Flatpak and is not a substitute for real
testing, but it can be much quicker when debugging as you can execute code
directly from the source tree. The following example works for me:

    bst shell tagcloud-test-environment.bst --mount ~/tagcloud /src \
        --mount /etc /etc --mount /home /home --mount /run /run \
        -- sh -c "cd /src/build; env HOME=$HOME ninja run-uninstalled"

You can also use this method to run an interactive `bash` session, and to run
the application under the `gdb` debugger.

Tracker domains
---------------

The Tracker indexer and database used by Tagcloud can be operated in several
ways. Originally the design was for a single session-wide set of Tracker
services that had knowledge of all the user's files. This is the most efficient
approach and allows for searches across all data stored by all applications.

However, this approach prevents any kind of fine-grained sandboxing of
applications -- users either give an app access to all their data, or none. It
also means that all apps have to use whatever version of Tracker is provided
by the user's distribution which makes it hard to use any new features. The
2.0 release of Tracker added a new
[domains](https://developer.gnome.org/libtracker-sparql/unstable/tracker-private-store.html)
feature which makes it practical for an app to have its own private instance
of the Tracker services.

Tagcloud uses the session-wide Tracker store by default but it also supports
using a private Tracker instance. This can be enabled by passing
`--tracker-domain app` on the commandline when starting Tagcloud.

If you followed the instructions above for installing Tagcloud into a custom
prefix like `/opt/tagcloud`, you will need to modify your D-Bus session bus
config so that the D-Bus .service files for the private Tracker instance can be
found. To achieve this I created a file named
`/etc/dbus-1/session.d/tagcloud.conf` with the following contents:

    <!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-Bus Bus Configuration 1.0//EN"
        "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
    <busconfig>
      <servicedir>/opt/tagcloud/share/dbus-1/services/</servicedir>
    </busconfig>

Plan
----

Here is a professionally-drafted UI mockup:

![](docs/design-1.0.jpg)

Here is an incomplete TODO list:

### 1.0

Filter content by containing folder, type.

Filter by text search

Open content in the relevant application from the content list's context menu

Page through long sets of content (allow viewing more than the first 100 results).

"Pretty" view of content list:

  * icon for type
  * filename hidden if title available
  * artist, title and album info shown for music
  * thumbnail / media art shown if possible
  * tags shown for each item

Sort content by Name, First Modified, Last Modified, Size, or Type.

Sort tags by Label, or Size.

Use a programmatic SPARQL query generator instead of blindly joining
strings together. (https://github.com/pudo/sparqlquery may be the best bet).

Provide a Flatpak

### 1.2

Store and edit notes about files.

Handle removable devices and folders outside the usual Tracker index (the
IndexFileForProcess call should be useful here).

Show tag filter as a path bar in the headerbar.

gnome-shell search provider for tags

Read and write tags as xattrs in the file system (using tracker-miner-fs and
tracker-writeback). Ideally be compatible with KDE's Baloo framework.

### 2.0

Support for remote sources:

  * last.fm -- import music tags
  * pinboard.in -- import document tags and notes

Importing from these should be done with a simple Tracker miner. This should be
done into a separate Tracker store, at least initially.

Credential storage should be done with gnome-online-accounts but we may need to
hack it in locally initially.

Tags from remote services should be visually distinct.

Writing tags and notes back to the remote services can be done too.

Suggestions for which tags to add based on existing information.

### Undecided

Show music content hierarchically, with artists or albums expanding to the
actual tracks. Expose this feature with a button next to Music in the Content
Type menu.

Heirarchical tags

Links to online databases for info on a given resource (e.g. Musicbrainz
for music)
