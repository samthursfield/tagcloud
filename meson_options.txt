option('default_tracker_domain', type: 'combo', choices: ['app', 'session'], value: 'app',
       description: 'Whether to use session-wide Tracker instance or an app-specific one.')
