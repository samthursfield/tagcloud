#!/usr/bin/env python3

import os
import subprocess

prefix = os.environ.get('MESON_INSTALL_PREFIX', '/usr/local')
iconcachedir = os.path.join(prefix, 'share', 'icons', 'hicolor')
schemadir = os.path.join(prefix, 'share', 'glib-2.0', 'schemas')

if not os.environ.get('DESTDIR'):
    print('Updating icon cache...')
    subprocess.call(['gtk-update-icon-cache', '-qtf', iconcachedir])

    print('Compiling gsettings schemas...')
    subprocess.call(['glib-compile-schemas', schemadir])
