# Tagcloud
#
# Copyright 2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import GLib, GObject

import logging

import tagcloud.locationmanager


class Notification(GObject.Object):
    '''Base class for notifications.'''
    @GObject.Signal
    def hide(self):
        '''Emitted when the notification should no longer be shown.'''
        pass


class RemovableDeviceNotification(Notification):
    '''Asks user whether they want to scan or ignore a removable device.'''
    def __init__(self, device):
        super(RemovableDeviceNotification, self).__init__()

        self.device = device
        self.device.connect('notify::action', lambda obj, pspec: self.on_action_changed())

    def on_action_changed(self):
        if self.device.get_action() != tagcloud.locationmanager.RemovableDeviceAction.ASK:
            self.emit('hide')

    def ignore(self):
        self.device.set_action(tagcloud.locationmanager.RemovableDeviceAction.IGNORE)

    def index(self):
        self.device.set_action(tagcloud.locationmanager.RemovableDeviceAction.INDEX)


class Notifications(GObject.Object):
    '''Manages interactive notifications that need to be shown to the user.'''

    def __init__(self):
        super(Notifications, self).__init__()

        self._list = []
        self._handlers = {}

    def __iter__(self):
        return self._list

    @GObject.Property(type=GLib.Array, default=[])
    def notifications(self):
        return self._list

    @GObject.Signal(arg_types=(GObject.Object,))
    def notification_added(self, n):
        handler = n.connect('hide', lambda n: self.remove(n))
        self._handlers[n] = handler

    @GObject.Signal(arg_types=(GObject.Object,))
    def notification_removed(self, n):
        GObject.signal_handler_disconnect(n, self._handlers[n])
        del self._handlers[n]

    def removable_device_notifications(self):
        return [n for n in self._list if isinstance(n, RemovableDeviceNotification)]

    def add_removable_device_notification(self, device):
        notification = RemovableDeviceNotification(device)
        self._list.append(notification)
        GLib.idle_add(lambda: self.emit('notification_added', notification))
        GLib.idle_add(lambda: self.notify('notifications'))

    def remove(self, notification):
        logging.debug("Removing notification {}".format(notification))
        self._list.remove(notification)
        GLib.idle_add(lambda: self.emit('notification_removed', notification))
        GLib.idle_add(lambda: self.notify('notifications'))
