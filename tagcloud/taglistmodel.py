# Tagcloud
#
# Copyright 2017 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Tracker', '2.0')
from gi.repository import Gio, GLib, GObject, Gtk, Tracker

import bisect

import tagcloud.pagedmodel


class TagItem(GObject.Object):
    def __init__(self, urn, label, n_children=None):
        super(TagItem, self).__init__()
        self.urn = urn
        self.label = label
        self.columns = [urn, label, n_children]


def count_tags_async(db, callback):
    query = 'SELECT COUNT(?tag) { ?tag a nao:Tag }'
    def query_next_callback(cursor, result):
        cursor.next_finish(result)
        callback(cursor.get_integer(0))

    def query_callback(obj, result):
        cursor = obj.query_finish(result)
        cursor.next_async(None, query_next_callback)
    db.query_async(query, None, query_callback)


def query_tag_async(db, tag_urn, callback):
    query = ''.join(
        ['SELECT ?label COUNT(?children) { ',
            '<%s> a nao:Tag ' % tag_urn,
                '; nao:prefLabel ?label . ',
            '?children nao:hasTag ?tag ',
         '}'])

    def query_next_callback(cursor, result):
        cursor.next_finish(result)
        callback(TagItem(
            urn=tag_urn,
            label=cursor.get_string(0)[0],
            n_children=cursor.get_string(1)[0],
        ))

    def query_callback(obj, result):
        cursor = obj.query_finish(result)
        cursor.next_async(None, query_next_callback)

    db.query_async(query, None, query_callback)


def query_tags_async(db, callback, offset=0, limit=100):
    query = ''.join(
        ['SELECT ?tag ?label COUNT(?children) { ',
            '?tag a nao:Tag ',
                '; nao:prefLabel ?label . ',
            '?children nao:hasTag ?tag ',
         '} ',
         'GROUP BY ?tag ',
         'ORDER BY ?label ',
         'OFFSET %i LIMIT %i' % (offset, limit)])

    def query_next_callback(cursor, result, items):
        if cursor.next_finish(result):
            item = TagItem(
                urn=cursor.get_string(0)[0],
                label=cursor.get_string(1)[0],
                n_children=cursor.get_string(2)[0],
            )
            items.append(item)
            cursor.next_async(None, query_next_callback, items)
        else:
            callback(items)

    def query_callback(obj, result):
        cursor = obj.query_finish(result)
        items = []
        cursor.next_async(None, query_next_callback, items)

    db.query_async(query, None, query_callback)


def query_tags_for_resource_async(db, resource_urn, callback, limit=10):
    query = ' '.join([
        'SELECT ?tag_urn ?tag_label {',
            '<%s> nao:hasTag ?tag_urn .' % resource_urn,
            '?tag_urn nao:prefLabel ?tag_label .',
        '}',
        'ORDER BY ?tag_label',
        'LIMIT %i' % limit,
    ])

    def query_next_callback(cursor, result, tags_for_resource):
        if cursor.next_finish(result):
            item = TagItem(
                urn=cursor.get_string(0)[0],
                label=cursor.get_string(1)[0],
            )
            tags_for_resource.append(item)
            cursor.next_async(None, query_next_callback, tags_for_resource)
        else:
            callback(tags_for_resource)

    def query_callback(obj, result):
        cursor = obj.query_finish(result)

        tags_for_resource = []
        cursor.next_async(None, query_next_callback, tags_for_resource)

    db.query_async(query, None, query_callback)


class TagListModel(tagcloud.pagedmodel.PagedTreeModel, Gtk.TreeModel):
    '''Represents the list of all known tags (or a filtered subset).

    Note that this is a Gtk.TreeModel rather than a Gio.TreeModel. We could
    quite possibly switch to Gtk.FlowBox backed by Gio.ListModel to display
    the tags rather than a Gtk.IconView.

    The list is broken up into pages. This is a bit of a kludge but it avoids
    any performance problems which would otherwise occur once we get beyond
    around 10,000 tags.

    '''
    def __init__(self, db, page_size=100):
        super(TagListModel, self).__init__(page_size)

        self.db = db

        self.tag_notifier = Tracker.Notifier.new(
            ['nao:Tag'],
            Tracker.NotifierFlags.NOTIFY_UNEXTRACTED | Tracker.NotifierFlags.QUERY_URN)
        self.tag_notifier.connect('events', self._on_tag_notifier_event)

        self.tag_urn_index = {}

        GLib.idle_add(self._populate)

    def _populate(self):
        def count_tags_callback(total_items):
            self.total_items = total_items
        count_tags_async(self.db, count_tags_callback)

        def query_tags_callback(items):
            for pos, tag in enumerate(items):
                self.tag_urn_index[tag.urn] = tag
            self.set_items(items)
        query_tags_async(self.db, query_tags_callback, offset=0,
                         limit=self.page_size)

    def _on_tag_notifier_event(self, notifier, events):
        for event in events:
            event_type = event.get_event_type()
            tag_urn = event.get_urn()

            if event_type in [Tracker.NotifierEventType.CREATE,
                              Tracker.NotifierEventType.UPDATE]:
                if tag_urn not in self.tag_urn_index:
                    def query_tag_callback(tag):
                        # FIXME: need to handle the case where it's not on our
                        # current page...
                        # Insert item at correct place in list. At present we
                        # always order by URI!
                        pos = bisect.bisect([tag.label for tag in self.items], tag.label)
                        self.tag_urn_index[tag_urn] = tag
                        self.insert_item(pos, tag)
                    query_tag_async(self.db, tag_urn, query_tag_callback)

            elif event_type == Tracker.NotifierEventType.DELETE:
                if tag_urn in self.tag_urn_index:
                    pos = self.items.index(self.tag_urn_index[tag_urn])
                    del self.tag_urn_index[tag_urn]
                    self.remove_item(pos)

    def do_get_n_columns(self):
        return 3

    def do_get_column_type(self, index):
        return str

    def do_get_value(self, iter, column):
        index = self._validate_and_parse_iter(iter)
        item = self.items[index]
        return item.columns[column]

    def text_column(self):
        return 1

    def path_list_to_tag_ids(self, paths):
        tag_uuids = []
        for path in paths:
            index = self._validate_and_parse_path(path)
            tag_uuids.append(self.items[index].urn)
        return tag_uuids


# We have a global TrackerNotifier for watching changes across all resources.
# This is probably more efficient than having 50 separate instances which are
# each watching the same GraphUpdated D-Bus signal.
_resource_notifier = None
def _global_resource_notifier():
    global _resource_notifier
    if not _resource_notifier:
        ALL_CLASSES = None
        _resource_notifier = Tracker.Notifier.new(
            classes=ALL_CLASSES, flags=Tracker.NotifierFlags.NONE, cancellable=None)
    return _resource_notifier


class TagsListModelForResource(GObject.Object, Gio.ListModel):
    '''Represents the list of tags that apply to a specific resource.'''
    def __init__(self, db, resource_id, resource_urn, page_size=10):
        super(TagsListModelForResource, self).__init__()

        self.db = db
        self.resource_id = resource_id
        self.resource_urn = resource_urn
        self.page_size = page_size

        self.items = []
        self._populate_idle = GLib.idle_add(self._populate)

        # FIXME: one notifier per resource ? this must have performance
        # implications, we should probably have a TrackerNotifier
        # subclass that only takes out a single D-Bus watch
        _global_resource_notifier().connect('events', self._on_notifier_event)

    def _populate(self):
        def query_tags_for_resource_callback(items):
            self.items_changed(position=0, removed=len(self.items), added=0)
            self.items = items
            self.items_changed(position=0, removed=0, added=len(self.items))

        query_tags_for_resource_async(self.db, self.resource_urn,
                                      query_tags_for_resource_callback,
                                      limit=self.page_size)
        self._populate_idle = None

    def _on_notifier_event(self, notifier, events):
        for event in events:
            event_type = event.get_event_type()

            if event_type == Tracker.NotifierEventType.UPDATE:
                if event.get_id() == self.resource_id:
                    if not self._populate_idle:
                        self._populate_idle = GLib.idle_add(self._populate)

    def do_get_n_items(self):
        return len(self.items)

    def do_get_item(self, position):
        return self.items[position]
