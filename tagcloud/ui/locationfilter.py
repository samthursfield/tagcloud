# Tagcloud
#
# Copyright 2017 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gio, GLib, GObject, Gtk


class LocationFilter(GObject.Object):
    '''Popover menu that allows filtering content by location.

    The class provides a 'locations' property that contains a list of
    Gio.File instances. You should connect to the
    'notify::locations' signal to handle changes to this list.

    '''

    def __init__(self):
        super(LocationFilter, self).__init__()

        self._location_manager = None

        self._all_locations = set()
        self._selected_locations = set()

        self.action_group = None
        self.actions = None
        self.popover = Gtk.Popover()

    def set_location_manager(self, location_manager):
        self._location_manager = location_manager

        self._location_manager.connect('notify::locations', lambda obj, pspec: self._locations_changed())

        self._all_locations = set(self._location_manager.locations)
        self._selected_locations = self._all_locations

        self._locations_changed()

    def _locations_changed(self):
        removed = self._all_locations.difference(self._location_manager.locations)
        added = set(self._location_manager.locations).difference(self._all_locations)

        self._all_locations = set(self._location_manager.locations)

        self._selected_locations.difference_update(removed)
        self._selected_locations.update(added)

        self.action_group = self._create_action_group()
        self.actions = []
        for action_name in self.action_group.list_actions():
            action = self.action_group.lookup_action(action_name)
            action.set_state(GLib.Variant('b', action.location in self._selected_locations))
            self.actions.append(action)

        box = Gtk.VBox()
        for n, location in enumerate(self._all_locations):
            item = Gtk.CheckButton.new_with_label(location.path)
            item.set_action_name('location-filter.%s' % n)
            box.pack_start(item, expand=False, fill=True, padding=4)

        box.show_all()

        self._update_popover(self.action_group, box)

        self.notify('selected_locations')
        self.notify('all_locations')

    @GObject.Property()
    def selected_locations(self):
        return list(sorted(self._selected_locations))

    @GObject.Property()
    def all_locations(self):
        return list(sorted(self._all_locations))

    def on_change_state(self, changed_action, state):
        changed_action.set_state(state)
        self._selected_locations = set(a.location for a in self.actions if a.get_state())
        self.notify('selected_locations')

    def _create_action_group(self):
        action_group = Gio.SimpleActionGroup()
        for n, location in enumerate(self._all_locations):
            action = Gio.SimpleAction.new_stateful(
                str(n), None, GLib.Variant('b', True))
            action.location = location
            action.connect('change-state', self.on_change_state)
            action_group.add_action(action)
        return action_group

    def _update_popover(self, action_group, widget):
        existing_child = self.popover.get_child()
        if existing_child:
            self.popover.remove(existing_child)

        self.popover.insert_action_group('location-filter', action_group)
        self.popover.add(widget)
