# Tagcloud
#
# Copyright 2017 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gdk, Gtk

from gettext import gettext as _


GDK_EVENT_PROPAGATE = False
GDK_EVENT_STOP = True


class TagsGrid(Gtk.IconView):
    '''A widget displaying a 2D grid of tags, with contextual action menu.'''
    def __init__(self):
        super(TagsGrid, self).__init__()
        self.set_selection_mode(Gtk.SelectionMode.MULTIPLE)

        self.delete_menu_item = Gtk.MenuItem.new_with_label(_('Delete selected tag'))

        self.menu = Gtk.Menu.new()
        self.menu.append(self.delete_menu_item)
        self.menu.show_all()

        self.do_selection_changed()

    def do_button_press_event(self, event):
        # GtkTreeView implements the 'popup' signal itself, but GtkIconView
        # doesn't...
        if event.triggers_context_menu():
            self.show_context_menu(event)
            return GDK_EVENT_STOP
        else:
            return Gtk.IconView.do_button_press_event(self, event)

    def do_selection_changed(self):
        items = self.get_selected_items()
        if len(items) == 0:
            self.delete_menu_item.set_label(_('Delete selected tag'))
            self.delete_menu_item.set_sensitive(False)
        elif len(items) == 1:
            self.delete_menu_item.set_label(_('Delete selected tag'))
            self.delete_menu_item.set_sensitive(True)
        else:
            self.delete_menu_item.set_label(_('Delete selected tags'))
            self.delete_menu_item.set_sensitive(True)

    def show_context_menu(self, button_press_event=None):
        if button_press_event:
            self.menu.popup(None, None, None, None, button_press_event.button,
                            button_press_event.time)
        else:
            self.menu.popup(None, None, None, None, None,
                            Gdk.get_current_event_time())

    def set_model(self, model):
        self.model = model
        Gtk.IconView.set_model(self, model)
        self.set_text_column(model.text_column())

    def set_actions_controller(self, actions):
        def do_delete_tags(menu_item):
            actions.delete_tags(self.get_selected_tag_ids())
        self.delete_menu_item.connect('activate', do_delete_tags)

    def get_selected_tag_ids(self):
        return self.model.path_list_to_tag_ids(
            self.get_selected_items())
