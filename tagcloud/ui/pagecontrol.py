# Tagcloud
#
# Copyright 2017 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import GObject, Gtk

from gettext import gettext as _


class PageControl(Gtk.Box):
    __initialized = False

    def __new__(cls, *args, **kwargs):
        if not cls.__initialized:
            cls.set_template_from_resource('/uk/me/afuera/Tagcloud/ui/page-control.ui')

            for child_name in ['select-page-button', 'text', 'previous-button',
                            'next-button']:
                cls.bind_template_child_full(child_name, internal_child=True,
                                            struct_offset=0)
            cls.__initialized = True

        return Gtk.Widget.__new__(cls, *args, **kwargs)

    def __init__(self):
        super(PageControl, self).__init__()
        self.init_template()

        self.label = self.get_template_child(type(self), 'text')

        self.select_page_button = self.get_template_child(type(self), 'select-page-button')
        self.select_page_button.connect('clicked', self.on_select_page_button_clicked)

        # We should open a text entry for entering the exact page number; but
        # that's not yet implemented so for now the button does nothing.
        self.select_page_button.set_sensitive(False)

        self.previous_button = self.get_template_child(type(self), 'previous-button')
        self.previous_button.connect('clicked', self.on_previous_button_clicked)

        self.next_button = self.get_template_child(type(self), 'next-button')
        self.next_button.connect('clicked', self.on_next_button_clicked)

        self._page_count = 0
        self._current_page = 0
        self.set_page_count(1)
        self.set_current_page(1)

    def get_current_page(self):
        return self._current_page

    def set_current_page(self, value):
        if value != self._current_page:
            if value < 1 or value > self._page_count:
                raise ValueError("Page %i outside range [%i, %i]" % (value, 1, self._page_count))

            self._current_page = value

            self.previous_button.set_sensitive((value != 1))
            self.next_button.set_sensitive((value < self._page_count))
            self.label.set_text(_("Page %i of %i" % (value, self._page_count)))

            self.notify('current_page')


    current_page = GObject.Property(type=int, minimum=1, default=1,
                                    getter=get_current_page,
                                    setter=set_current_page)

    def get_page_count(self):
        return self._page_count

    def set_page_count(self, value):
        if self._page_count != value:
            self._page_count = value

            self.next_button.set_sensitive((self._current_page < self._page_count))
            self.label.set_text(_("Page %i of %i" % (self._current_page, self._page_count)))

            if self._current_page > self._page_count:
                self.set_current_page(self._page_count)

            self.notify('page_count')

    page_count = GObject.Property(type=int, minimum=1, default=1,
                                  getter=get_page_count, setter=set_page_count)

    def on_select_page_button_clicked(self, button):
        pass  # Not implemented yet

    def on_previous_button_clicked(self, button):
        if self._current_page > 1:
            self.set_current_page(self._current_page - 1)

    def on_next_button_clicked(self, button):
        if self._current_page < self._page_count:
            self.set_current_page(self._current_page + 1)
