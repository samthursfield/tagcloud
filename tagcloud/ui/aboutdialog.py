# Tagcloud
#
# Copyright 2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gtk, GObject

from gettext import gettext as _


class AboutDialog(Gtk.AboutDialog):
    def __init__(self, app_version):
        super(AboutDialog, self).__init__()
        self.set_comments(_("Organize your junk using tags!"))
        self.set_license_type(Gtk.License.GPL_3_0)
        self.set_logo_icon_name('uk.me.afuera.Tagcloud')
        self.set_program_name(_("Tagcloud"))
        self.set_title(_("About Tagcloud"))
        self.set_version(app_version)

