# Tagcloud
#
# Copyright 2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gtk

import logging

import tagcloud.notifications


class NotificationOverlay(Gtk.Revealer):
    '''Displays notifications from the notificationmanager class to the user.'''

    def __init__(self, manager):
        super(NotificationOverlay, self).__init__()

        self.manager = manager

        # Show us at the top centre of the content listbox (using GtkOverlay).
        self.set_halign(Gtk.Align.CENTER)
        self.set_valign(Gtk.Align.START)

        # Frame with a transparent gray background.
        frame = Gtk.Frame.new()
        context = frame.get_style_context()
        context.add_class('app-notification')
        self.add(frame)

        # Box to contain notifications.
        self.box = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=4)
        frame.add(self.box)

        self.set_reveal_child(False)

        self._widgets = {}

        for n in self.manager.notifications:
            self._add_notification(n)

        self.manager.connect('notification_added', lambda manager, n: self._add_notification(n))
        self.manager.connect('notification_removed', lambda manager, n: self._remove_notification(n))

    def _add_notification(self, n):
        if isinstance(n, tagcloud.notifications.RemovableDeviceNotification):
            builder = Gtk.Builder()
            builder.add_from_resource('/uk/me/afuera/Tagcloud/ui/notification-new-device.ui')

            widget = builder.get_object('new-device-grid')

            device_label = builder.get_object('new-device-name')
            device_label.set_text(n.device.name())

            scan_button = builder.get_object('new-device-scan-button')
            scan_button.connect('clicked', lambda obj: n.index())

            scan_button = builder.get_object('new-device-ignore-button')
            scan_button.connect('clicked', lambda obj: n.ignore())
        else:
            logging.warn("Cannot display notification of type: {}")
            return

        self._widgets[n] = widget

        self.box.pack_start(widget, False, True, 0)
        widget.show_all()
        self.show_all()

        self.set_reveal_child(True)

    def _remove_notification(self, n):
        if n not in self._widgets:
            logging.warn("Cannot remove notification {}: not in list".format(n))
            return

        widget = self._widgets[n]
        del self._widgets[n]

        if len(self._widgets) == 0:
            self.set_reveal_child(False)

        self.box.remove(widget)
