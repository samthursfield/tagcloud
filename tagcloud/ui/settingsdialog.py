# Tagcloud
#
# Copyright 2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gtk, GObject

from gettext import gettext as _


class LocationsPage():
    '''Manages the 'Locations' page in the settings stack.'''
    def __init__(self, app, builder, location_manager):
        self.list_box = builder.get_object('locations-list-box')

        add_location_button = builder.get_object('add-location-button')
        add_location_button.connect('clicked', lambda button: self._on_add_location())

        self.location_manager = location_manager

        self.location_manager.connect('notify::locations', lambda obj, pspec: self._update_list())
        self._update_list()

        # An attempt to keep the user informed about Tracker settings; not that
        # this message will make sense to most people but at least they can
        # tell the difference between the two 'modes' that we currently offer.
        locations_domain_note = builder.get_object('locations-domain-note')
        domain_name = app.tracker_domain_name()
        if domain_name == 'uk.me.afuera.Tagcloud':
            locations_domain_note.set_text(_(
                "Changes here affect the {} Tracker domain. This domain "
                "is private to Tagcloud so no other apps will be affected."
                .format(domain_name)))
        else:
            # FIXME: We should possibly take into account whether we are
            # running inside a Flatpak container that doesn't share DConf,
            # too. However most Flatpak apps seem to share the session DConf
            # database at present anyway, and in these cases the message is
            # correct that changes affect the user's whole session.
            locations_domain_note.set_text(_(
                "Changes here affect the {} Tracker domain. Other apps that "
                "use the same Tracker domain will be affected."
                .format(domain_name)))

    def _update_list(self):
        for child in self.list_box.get_children():
            self.list_box.remove(child)

        for location in self.location_manager.locations:
            builder = Gtk.Builder()
            builder.add_objects_from_resource('/uk/me/afuera/Tagcloud/ui/settings-dialog.ui', ['locations-list-item'])

            item = builder.get_object('locations-list-item')

            path_label = builder.get_object('locations-list-item-path')
            path_label.set_text(location.path)

            recursive_checkbutton = builder.get_object('locations-list-item-recursive')
            recursive_checkbutton.set_active(location.recursive)
            recursive_checkbutton.set_sensitive(False)

            remove_button = builder.get_object('locations-list-item-remove')
            if location.can_remove:
                def remove_location(button, path):
                    self.location_manager.remove_location(path)
                remove_button.connect('clicked', remove_location, location.path)
            else:
                remove_button.set_sensitive(False)

            self.list_box.insert(item, position=-1)

    def _on_add_location(self):
        file_chooser_dialog = Gtk.FileChooserDialog(
            _("Tagcloud - Add Content Location"), self,
            Gtk.FileChooserAction.SELECT_FOLDER)
        file_chooser_dialog.add_buttons(
            _("Cancel"), Gtk.ResponseType.CANCEL, _("Add"), Gtk.ResponseType.ACCEPT)

        result = file_chooser_dialog.run()
        if result == Gtk.ResponseType.ACCEPT:
            self.location_manager.add_location(file_chooser_dialog.get_filename())
        file_chooser_dialog.destroy()


class DevicesPage():
    '''Manages the 'Devices' page in the settings stack.'''
    def __init__(self, builder, location_manager):
        self.list_box = builder.get_object('devices-list-box')

        self.location_manager = location_manager

        self.location_manager.connect('notify::removable-devices', lambda obj, pspec: self._update_list())
        self._update_list()

    def _update_list(self):
        for child in self.list_box.get_children():
            self.list_box.remove(child)

        for device in self.location_manager.removable_devices:
            builder = Gtk.Builder()
            builder.add_objects_from_resource('/uk/me/afuera/Tagcloud/ui/settings-dialog.ui', ['devices-list-item'])

            item = builder.get_object('devices-list-item')

            name_label = builder.get_object('devices-list-item-name')
            name_label.set_text(device.name())

            action_widget = builder.get_object('devices-list-item-action')
            action_widget.set_active(device.get_action().value)
            action_widget.bind_property('active', device, 'action', GObject.BindingFlags.BIDIRECTIONAL)

            self.list_box.insert(item, position=-1)


class SettingsDialog(Gtk.Dialog):
    def __init__(self, app, location_manager):
        Gtk.Dialog.__init__(self, title=_("Tagcloud Settings"))

        builder = Gtk.Builder()
        builder.add_from_resource('/uk/me/afuera/Tagcloud/ui/settings-dialog.ui')

        # Replace the outer widget from GtkBuilder with our own. Nasty!
        contents = builder.get_object('settings-dialog-contents')
        builder.get_object('settings-dialog').get_content_area().remove(contents)
        self.get_content_area().pack_start(contents, expand=True, fill=True, padding=0)
        self.get_content_area().show_all()

        self.locations = LocationsPage(app, builder, location_manager)
        self.devices = DevicesPage(builder, location_manager)
