# Tagcloud
#
# Copyright 2017-2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import humanize

from gi.repository import Gdk, GdkPixbuf, Gio, GLib, Gtk

import collections
import datetime
import logging
import os
import urllib.parse

import tagcloud.content


def abbreviate(text, max_length=10):
    assert max_length > 3
    if len(text) > max_length:
        halfway = int(max_length / 2)
        start = text[0:halfway-1]
        end = text[-(halfway-1):]
        text = start + '...' + end
    return text


def open_url(url):
    context = Gdk.Display.get_default().get_app_launch_context()
    try:
       Gio.AppInfo.launch_default_for_uri(url, context)
    except GLib.Error as e:
        logging.warn("Error opening %s: %s" % (url, e))


def open_in_file_manager(path):
    context = Gdk.Display.get_default().get_app_launch_context()
    url = urllib.parse.urlunsplit(
        ('file', urllib.parse.quote(path), '', '', ''))
    try:
        Gio.AppInfo.launch_default_for_uri(url, context)
    except GLib.Error as e:
        logging.warn("Error opening %s: %s" % (url, e))


class ContentInfoPopover(Gtk.Popover):
    VALUE_MAX_WIDTH = 32
    def __init__(self):
        super(ContentInfoPopover, self).__init__()

        self.item = None

        self.item_change_handler_id = None

        self.grid = Gtk.Grid.new()
        self.grid.set_row_spacing(6)
        self.grid.set_column_spacing(6)

        self.add(self.grid)

        self.column_cursors = collections.defaultdict(int)

    def reset(self):
        self.grid.foreach(lambda w: self.grid.remove(w))
        self.column_cursors = collections.defaultdict(int)

    def attach_field(self, column, name, value_widget):
        row = self.column_cursors[column]
        self.column_cursors[column] += 1

        key_label = Gtk.Label.new(name)
        key_label.set_halign(Gtk.Align.END)
        self.grid.attach(key_label, column, row, 1, 1)
        self.grid.attach(value_widget, column + 1, row, 1, 1)

    def n_rows(self):
        return max(self.column_cursors.values())

    def on_item_metadata_changed(self, item):
        def value_label(text):
            widget = Gtk.Label.new(text)
            widget.set_halign(Gtk.Align.START)
            return widget

        if item == self.item:
            self.reset()
            print("Item metadata: %s" % self.item.metadata)

            filename = abbreviate(item.file_name, max_length=self.VALUE_MAX_WIDTH)
            filename_button = Gtk.Button.new_with_label(filename)
            filename_button.set_relief(Gtk.ReliefStyle.NONE)
            filename_button.set_halign(Gtk.Align.START)
            filename_button.connect('clicked', lambda w: open_url(item.url))
            self.attach_field(1, 'Filename', filename_button)

            path = abbreviate(item.path, max_length=self.VALUE_MAX_WIDTH)
            path_button = Gtk.Button.new_with_label(path)
            path_button.set_relief(Gtk.ReliefStyle.NONE)
            path_button.set_halign(Gtk.Align.START)
            path_button.connect('clicked', lambda w: open_in_file_manager(item.path))
            self.attach_field(1, 'Path', path_button)

            if item.metadata.get('size'):
                size = humanize.naturalsize(item.metadata['size'])
            else:
                size = ''
            size_widget = value_label(size)
            self.attach_field(1, 'Size', size_widget)

            if item.metadata.get('created'):
                created = humanize.naturaltime(
                    datetime.datetime.strptime(item.metadata['created'],
                                               '%Y-%m-%dT%H:%M:%SZ'))
            else:
                created = ''
            created_widget = value_label(created)
            self.attach_field(1, 'Created', created_widget)

            if item.metadata.get('modified'):
                modified = humanize.naturaltime(
                    datetime.datetime.strptime(item.metadata['modified'],
                                               '%Y-%m-%dT%H:%M:%SZ'))
            else:
                modified = ''
            modified_widget = value_label(modified)
            self.attach_field(1, 'Modified', modified_widget)

            if item.content_type == tagcloud.content.Types.IMAGE.value:
                try:
                    pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
                        os.path.join(item.path, item.file_name),
                        width=256, height=256, preserve_aspect_ratio=True)
                    image = Gtk.Image.new_from_pixbuf(pixbuf)
                except GLib.Error as e:
                    if e.domain in [GLib.file_error_quark(), GdkPixbuf.pixbuf_error_quark()]:
                        logging.warn("Couldn't load {}: {}".format(item.path), e)
                        image = Gtk.Image.new_from_icon_name('image-missing', Gtk.IconSize.DIALOG)
                    else:
                        raise
                self.grid.attach(image, 0, 0, 1, self.n_rows())
            else:
                # Only images have a special preview field, for now.
                pass

            self.grid.show_all()

            if self.item_change_handler_id:
                self.item.disconnect(self.item_change_handler_id)
                self.item_change_handler_id = None

    def set_item(self, item):
        if item != self.item:
            if self.item_change_handler_id:
                self.item.disconnect(self.item_change_handler_id)
                self.item_change_handler_id = None

            self.item = item
            self.on_item_metadata_changed(item)

            if not item.has_full_metadata:
                item.query_full_metadata_async()
                self.item_change_handler_id = item.connect(
                    'metadata_changed', self.on_item_metadata_changed)
