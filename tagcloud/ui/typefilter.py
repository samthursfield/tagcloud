# Tagcloud
#
# Copyright 2017 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import GLib, Gio, GObject, Gtk

import tagcloud.content


class TypeFilter(GObject.Object):
    '''Popover menu that allows filtering content by type.

    The class provides a 'types' property that contains a list of
    values from tagcloud.content.Types. You should connect to the
    'notify::types' signal to handle changes to this list.

    '''
    def __init__(self):
        super(TypeFilter, self).__init__()

        self._types = tagcloud.content.Types

        self.action_group = self._create_action_group()
        self.actions = [
            self.action_group.lookup_action(action_name)
            for action_name in self.action_group.list_actions()]

        self.popover = self._create_popover(self.action_group)

    @GObject.Property()
    def types(self):
        return self._types

    def on_change_state(self, action, state):
        action.set_state(state)
        self._types = [a.content_type for a in self.actions if a.get_state()]
        self.notify('types')

    def _create_action_group(self):
        action_group = Gio.SimpleActionGroup()
        for content_type in tagcloud.content.Types:
            action = Gio.SimpleAction.new_stateful(
                '%s' % content_type.name, None, GLib.Variant('b', True))
            action.content_type = content_type
            action.connect('change-state', self.on_change_state)
            action_group.add_action(action)
        return action_group

    def _create_popover(self, action_group):
        popover = Gtk.PopoverMenu()
        popover.insert_action_group('type-filter', action_group)

        box = Gtk.VBox()
        for content_type in tagcloud.content.Types:
            item = Gtk.CheckButton.new_with_label(content_type.value['label'])
            item.set_action_name('type-filter.%s' % content_type.name)
            box.pack_start(item, expand=False, fill=True, padding=4)

        box.show_all()
        popover.add(box)
        return popover
