# Tagcloud
#
# Copyright 2017-2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gtk

import tagcloud.ui.contentinfopopover

GTK_STYLE_PROVIDER_PRIORITY_APPLICATION = 600


class ItemThumbnailClosure(object):
    '''Handles asynchronous loading of thumbnails.'''
    def __init__(self, thumbnail_image, item):
        self.thumbnail_image = thumbnail_image
        self.item = item

        self.handler_id = item.connect('notify::thumbnail', self.on_thumbnail_changed)

        self.set_thumbnail(None)

    def set_thumbnail(self, thumbnail_pixbuf):
        if thumbnail_pixbuf:
            self.thumbnail_image.set_from_pixbuf(thumbnail_pixbuf)
        else:
            self.thumbnail_image.set_from_icon_name(self.item.content_type['icon-name'],
                                                    self.item.thumbnail_height())

    def on_thumbnail_changed(self, item, pspec):
        self.set_thumbnail(item.thumbnail)
        self.remove_signal()

    def remove_signal(self):
        self.item.disconnect(self.handler_id)
        self.handler_id = None


class ContentListBox(Gtk.ListBox):
    '''A widget displaying the list of content items.'''
    def __init__(self):
        super(ContentListBox, self).__init__()

        self.content_info_popover = tagcloud.ui.contentinfopopover.ContentInfoPopover()
        self.content_info_popover.set_position(Gtk.PositionType.BOTTOM)

        self.tag_label_style_provider = Gtk.CssProvider.new()
        self.tag_label_style_provider.load_from_data(
            b'label { background-color: rgb(200, 200, 200); padding-left: 6px; padding-right: 6px; }')

        self.thumbnail_size_group = Gtk.SizeGroup.new(Gtk.SizeGroupMode.HORIZONTAL)

        self.set_selection_mode(Gtk.SelectionMode.MULTIPLE)
        self.set_activate_on_single_click(False)

        self.model = None

    def get_selected_items(self):
        return [row.get_child().item for row in self.get_selected_rows()]

    def _create_item_cb(self, item, user_data):
        builder = Gtk.Builder.new_from_resource('/uk/me/afuera/Tagcloud/ui/content-list-item.ui')
        widget = builder.get_object('content-list-item')
        widget.item = item

        info_button = builder.get_object('content-info-button')
        info_button.connect('clicked', self.show_content_info_popover, item)

        title = builder.get_object('content-title')
        title.set_label(item.title)

        tags = builder.get_object('content-tags')
        tags.bind_model(item.tags_model, self._create_tag_for_item, None)

        thumbnail_image = builder.get_object('content-thumbnail-image')

        self.thumbnail_size_group.add_widget(thumbnail_image)

        # Handler to set the thumbnail image if/when it is loaded. Hopefully by
        # storing this on the widget we ensure it all gets freed correctly.
        widget.thumbnail_closure = ItemThumbnailClosure(thumbnail_image, item)

        widget.show_all()
        return widget

    def _create_tag_for_item(self, tag, user_data):
        label = Gtk.Label.new(tag.label)
        label.set_width_chars(3)
        label.set_halign(Gtk.Align.CENTER)
        label.get_style_context().add_provider(self.tag_label_style_provider,
                                               GTK_STYLE_PROVIDER_PRIORITY_APPLICATION)
        return label

    def set_model(self, content_list_model):
        self.model = content_list_model
        self.bind_model(content_list_model, self._create_item_cb, None)

    def show_content_info_popover(self, button, item):
        self.content_info_popover.set_relative_to(button)
        self.content_info_popover.set_item(item)
        self.content_info_popover.popup()
