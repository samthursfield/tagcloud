# Tagcloud
#
# Copyright 2017-2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gdk, Gio, GLib, GObject, Gtk

import logging
from gettext import gettext as _

import tagcloud.actions
import tagcloud.content
import tagcloud.ui.contentlistbox
import tagcloud.ui.locationfilter
import tagcloud.ui.notificationoverlay
import tagcloud.ui.pagecontrol
import tagcloud.ui.tagsgrid
import tagcloud.ui.typefilter


GDK_EVENT_PROPAGATE = False
GDK_EVENT_STOP = True


class Menu(Gtk.Popover):
    def __init__(self):
        super(Menu, self).__init__()

        model = Gio.Menu()
        model.append('Settings', 'app.settings')
        model.append('About', 'app.about')

        self.bind_model(model)


class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, app, notification_manager):
        '''Contruct the UI.

        Connecting to the Tracker database can potentially be slow, so this
        class can be constructed without you needing to provide the actual data
        models and action controllers that connect to the database.

        One a database connection has been made, you should call
        set_content_list_model(), set_tag_list_model() and
        set_actions_controller().

        '''
        Gtk.ApplicationWindow.__init__(self,
                                       application=app,
                                       title=_("Tagcloud"))
        self.app = app

        self._settings = Gio.Settings.new('uk.me.afuera.Tagcloud')

        self._bind_window_state_to_settings(self._settings)

        builder = Gtk.Builder()
        builder.add_from_resource('/uk/me/afuera/Tagcloud/ui/main-window.ui')

        window = builder.get_object('main-window')
        headerbar = window.get_titlebar()
        window.remove(headerbar)
        self.set_titlebar(headerbar)

        child = window.get_child()
        window.remove(child)

        self.content_list_model = None
        self.content_list_box = tagcloud.ui.contentlistbox.ContentListBox()
        self.content_list_box.connect('selected-rows-changed', self.on_content_selection_changed)
        self.content_action_bar = builder.get_object('content-action-bar')
        content_list_box_container = builder.get_object('content-list-box-container')
        content_list_box_container.add(self.content_list_box)

        # Placeholders for action widgets which are created once we have an
        # action controller.
        self.add_tag_button = None
        self.trash_button = None

        self.tags_grid = tagcloud.ui.tagsgrid.TagsGrid()
        self.tags_grid.connect('selection-changed', self.on_tag_selection_changed)
        tags_grid_container = builder.get_object('tags-grid-container')
        tags_grid_container.add(self.tags_grid)

        self.type_filter = tagcloud.ui.typefilter.TypeFilter()
        self.type_filter_menubutton = builder.get_object('type-filter-menubutton')
        self.type_filter_menubutton.set_popover(self.type_filter.popover)
        self.type_filter_menubutton_label = builder.get_object('type-filter-menubutton-label')

        self.location_filter = tagcloud.ui.locationfilter.LocationFilter()
        self.location_filter_menubutton = builder.get_object('location-filter-menubutton')
        self.location_filter_menubutton.set_popover(self.location_filter.popover)
        self.location_filter_menubutton_label = builder.get_object('location-filter-menubutton-label')

        self.text_filter = builder.get_object('search-entry')

        self.on_content_selection_changed(self.content_list_box)

        self.add(child)
        child.show_all()

        self.menu_button = builder.get_object('menu-button')
        self.menu_button.set_popover(Menu())

        self.notification_overlay = tagcloud.ui.notificationoverlay.NotificationOverlay(notification_manager)

        self.status_box = builder.get_object('status-box')
        self.status_label = builder.get_object('status-label')
        self.status_spinner = builder.get_object('status-spinner')
        self.set_indexing_status(active=False)

        self.content_list_box_overlay = builder.get_object('content-list-box-overlay')
        self.content_list_box_overlay.add_overlay(self.notification_overlay)
        self.content_list_box_overlay.add_overlay(self.status_box)

        self._setup_keyboard_shortcuts()

    def _setup_keyboard_shortcuts(self):
        accel_group = Gtk.AccelGroup()
        self.add_accel_group(accel_group)

        key, mod = Gtk.accelerator_parse('<Control>S')
        accel_group.connect(key, mod, Gtk.AccelFlags.VISIBLE, lambda *args: self.app.lookup_action('settings').activate())

    def _setup_content_action_bar(self, builder, action_bar, actions):
        # FIXME: I can't figure out how to create the button in the .ui file
        self.add_tag_button = Gtk.Button.new_with_label("Add tag")

        add_tag_popover = builder.get_object('add-tag-popover')
        add_tag_popover.set_relative_to(self.add_tag_button)

        add_tag_entry = builder.get_object('add-tag-entry')
        add_tag_cancel = builder.get_object('add-tag-cancel')
        add_tag_ok = builder.get_object('add-tag-ok')

        def on_add_tag():
            actions.add_tag(self.selected_items, add_tag_entry.get_text())

        add_tag_cancel.connect('clicked', lambda b: add_tag_popover.popdown())
        add_tag_ok.connect('clicked', lambda b: add_tag_popover.popdown())
        add_tag_ok.connect('clicked', lambda b: on_add_tag())
        add_tag_entry.connect('activate', lambda e: add_tag_popover.popdown())
        add_tag_entry.connect('activate', lambda e: on_add_tag())

        self.add_tag_button.connect('clicked', lambda b: add_tag_popover.popup())

        action_bar.pack_start(self.add_tag_button)

        def on_trash_items(menu_item):
            actions.trash_items(self.content_list_box.get_selected_items())
        self.trash_button = Gtk.Button.new_with_label(_("Move to Trash"))
        self.trash_button.connect('clicked', on_trash_items)

        action_bar.pack_start(self.trash_button)

        content_list_page_control = tagcloud.ui.pagecontrol.PageControl()
        self.content_list_model.bind_property('page_count',
                                              content_list_page_control,
                                              'page_count',
                                              GObject.BindingFlags.DEFAULT)
        self.content_list_model.bind_property('current_page',
                                              content_list_page_control,
                                              'current_page',
                                              GObject.BindingFlags.BIDIRECTIONAL)
        action_bar.pack_end(content_list_page_control)

        action_bar.show_all()

        self.on_content_selection_changed(self.content_list_box)

    def _bind_window_state_to_settings(self, settings):
        size_setting = settings.get_value('window-size')
        logging.debug("Saved window-size: %s" % (size_setting))
        if isinstance(size_setting[0], int) and isinstance(size_setting[1], int):
            self.resize(size_setting[0], size_setting[1])

        position_setting = settings.get_value('window-position')
        logging.debug("Saved window-position: %s" % (position_setting))
        if len(position_setting) == 2 \
           and isinstance(position_setting[0], int) \
           and isinstance(position_setting[1], int):
            self.move(position_setting[0], position_setting[1])

        if settings.get_value('window-maximized'):
            self.maximize()

        def store_window_size_and_position(widget):
            size = widget.get_size()
            settings.set_value('window-size', GLib.Variant('ai', [size[0], size[1]]))

            # FIXME: this is useless on Wayland because there's no absolute
            # coordinate system available to clients. I can't find any
            # information on how to get gnome-shell to remember our position
            # for us, though :-(
            pos = widget.get_position()
            settings.set_value('window-position', GLib.Variant('ai', [pos[0], pos[1]]))
            GLib.source_remove(self._window_size_update_timeout)
            self._window_size_update_timeout = None
            return False

        def on_configure_event(widget, event):
            if self._window_size_update_timeout is None:
                self._window_size_update_timeout = GLib.timeout_add(
                    500, store_window_size_and_position, widget)

        def on_window_state_event(widget, event):
            settings.set_boolean('window-maximized',
                                 'GDK_WINDOW_STATE_MAXIMIZED' in event.new_window_state.value_names)

        self._window_size_update_timeout = None
        self.connect('window-state-event', on_window_state_event)
        self.connect('configure-event', on_configure_event)

    def handle_type_filter_change(self, type_filter):
        if self.content_list_model:
            self.content_list_model.set_content_type_filter(type_filter)

        if len(type_filter) == 0:
            self.type_filter_menubutton_label.set_text("No Types")
        elif len(type_filter) == 1:
            self.type_filter_menubutton_label.set_text("Only %s" % type_filter[0].value['label'])
        elif len(type_filter) < len(tagcloud.content.Types):
            self.type_filter_menubutton_label.set_text("%i Types" % len(type_filter))
        else:
            self.type_filter_menubutton_label.set_text("All Types")

    def handle_location_filter_change(self, selected_locations, all_locations):
        if self.content_list_model:
            self.content_list_model.set_location_filter(selected_locations)

        n_selected = len(selected_locations)
        n_all = len(all_locations)
        if n_selected == n_all:
            self.location_filter_menubutton_label.set_text("All Locations")
        elif n_selected == 1:
            self.location_filter_menubutton_label.set_text("Only %s" % selected_locations[0].path)
        else:
            self.location_filter_menubutton_label.set_text("%i Locations" % n_selected)

    def handle_text_filter_change(self, text_filter):
        if self.content_list_model:
            self.content_list_model.set_text_filter(text_filter)

    def set_content_list_model(self, content_list_model):
        self.content_list_model = content_list_model
        self.content_list_box.set_model(content_list_model)

        def notify_type_filter_change(type_filter, pspec):
            self.handle_type_filter_change(type_filter.get_property('types'))
        self.type_filter.connect('notify::types', notify_type_filter_change)
        self.handle_type_filter_change(self.type_filter.get_property('types'))

        def notify_location_filter_change(location_filter, pspec):
            self.handle_location_filter_change(location_filter.get_property('selected_locations'),
                                               location_filter.get_property('all_locations'))
        self.location_filter.connect('notify', notify_location_filter_change)
        self.handle_location_filter_change(self.location_filter.get_property('selected_locations'),
                                           self.location_filter.get_property('all_locations'))

        def notify_text_filter_change(search_entry):
            self.handle_text_filter_change(search_entry.get_text())
        self.text_filter.connect('search-changed', notify_text_filter_change)
        self.handle_text_filter_change(self.text_filter.get_text())

    def set_tag_list_model(self, tag_list_model):
        self.tags_grid.set_model(tag_list_model)

    def set_actions_controller(self, actions):
        builder = Gtk.Builder()
        builder.add_from_resource('/uk/me/afuera/Tagcloud/ui/main-window.ui')
        self._setup_content_action_bar(builder, self.content_action_bar, actions)
        self.tags_grid.set_actions_controller(actions)

    def set_location_manager(self, location_manager):
        self.location_filter.set_location_manager(location_manager)

    def on_content_selection_changed(self, listbox):
        selection = self.content_list_box.get_selected_rows()
        self.selected_items = [widget.get_child().item for widget in selection]
        if self.add_tag_button:
            if len(selection) == 0:
                self.add_tag_button.set_sensitive(False)
            else:
                self.add_tag_button.set_sensitive(True)
        if self.trash_button:
            if len(selection) == 0:
                self.trash_button.set_sensitive(False)
            else:
                self.trash_button.set_sensitive(True)

    def on_tag_selection_changed(self, tags_grid):
        selected_tag_ids = self.tags_grid.get_selected_tag_ids()
        self.content_list_model.set_tag_filter(selected_tag_ids)

    def set_indexing_status(self, active, message=""):
        if active:
            self.status_label.set_text(message)
            self.status_box.show()
        else:
            self.status_label.set_text("")
            self.status_box.hide()
