#!/bin/sh
# Helper to run Tagcloud uninstalled from the build tree.
#
# Don't use this directly! Use `ninja run-uninstalled`.

set -eu

export GSETTINGS_SCHEMA_DIR=$MESON_BUILD_ROOT/data
export PYTHONPATH=$MESON_SOURCE_ROOT
export TAGCLOUD_DATA_DIR=$MESON_BUILD_ROOT/data

$@
