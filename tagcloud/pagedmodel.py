# Tagcloud
#
# Copyright 2017 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, GObject, Gtk


class PagedTreeModel(GObject.Object, Gtk.TreeModel):
    '''A list model that breaks data up into pages.

    This is a bit of a kludge but it avoids any performance problems which
    would otherwise occur once we get beyond around 10,000 items.

    '''
    def __init__(self, page_size):
        super(PagedTreeModel, self).__init__()

        self.page_size = page_size

        self.total_items = 0
        self.items = []

        self._iter_stamp = id(self) & 0x7FFFFFFF

    def _create_iter(self, index):
        iter = Gtk.TreeIter()
        iter.stamp = self._iter_stamp
        iter.user_data = index
        return iter

    def _validate_and_parse_iter(self, iter):
        if iter.stamp != self._iter_stamp:
            GLib.usleep(10)
            raise ValueError("Invalid iter: stamp was %i, expected %i" % (iter.stamp, id(self)))
        index = iter.user_data
        return index

    def _validate_and_parse_path(self, path):
        if path.get_depth() != 1:
            raise ValueError("Got path of depth %i" % (path.get_depth()))
        index = path.get_indices()[0]
        return index

    def set_items(self, items):
        '''Update the whole data set. Any existing items are removed.'''

        path = Gtk.TreePath.new_first()
        for i in self.items:
            self.row_deleted(path)
            path.next()

        self.items = items

        path = Gtk.TreePath.new_first()
        iter = self._create_iter(0)
        for item in self.items:
            self.row_inserted(path, iter)
            path.next()
            iter.user_data += 1

    def insert_item(self, position, item):
        self.items.insert(position, item)
        self.total_items += 1

        path = Gtk.TreePath.new_from_indices([position])
        iter = self._create_iter(position)
        self.row_inserted(path, iter)

    def remove_item(self, position):
        del self.items[position]
        self.total_items -= 1

        path = Gtk.TreePath.new_from_indices([position])
        self.row_deleted(path)

    def do_get_flags(self):
        # If you find yourself invalidating iters after a signal is
        # emitted, remove the ITERS_PERSIST flag !!
        return Gtk.TreeModelFlags.ITERS_PERSIST | Gtk.TreeModelFlags.LIST_ONLY

    def do_get_iter(self, path):
        index = self._validate_and_parse_path(path)
        if 0 <= index < len(self.items):
            return (True, self._create_iter(index))
        else:
            return (False, None)

    def do_get_path(self, iter):
        if iter is None:
            path = Gtk.TreePath.new_first()
        else:
            index = self._validate_and_parse_iter(iter)
            path = Gtk.TreePath(path=index)
        return path

    def do_iter_next(self, iter):
        if iter.user_data < (len(self.items) - 1):
            iter.user_data += 1
            return (True, iter)
        else:
            return (False, None)

    def do_iter_previous(self, iter):
        if iter.user_data > 0:
            iter.user_data -= 1
            return (True, iter)
        else:
            return (False, None)

    def do_iter_children(self, iter):
        if iter is None and len(self.items) > 0:
            return (True, self._create_iter(0))
        else:
            raise (False, None)

    def do_iter_has_child(self, iter):
        if iter is None and len(self.items) > 0:
            return True
        else:
            return False

    def do_iter_n_children(self, iter):
        if iter is None:
            return len(self.items)
        else:
            return 0

    def do_iter_nth_child(self, parent_iter, n):
        if parent_iter is None:
            if 0 <= n < len(self.items):
                return (True, self._create_iter(n))
            else:
                return (False, None)
        else:
            return (False, None)

    def do_iter_parent(self, child_iter):
        return (False, None)
