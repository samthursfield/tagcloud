# Tagcloud
#
# Copyright 2017 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import enum


class Types(enum.Enum):
    '''Types of content.'''

    AUDIO = {
        'label': 'Audio',
        # Tracker labels all audio as nmm:MusicPiece, even though users won't
        # necessarily consider all their audio files as music. Tracker labels
        # all video as nmm:Video which implies both nfo:Video and nfo:Audio,
        # thus we can'tfilter on nfo:Audio here because we'll include all the
        # videos too.
        'rdf-type': 'nmm:MusicPiece',
        'icon-name': 'audio-x-generic',
    }

    DOCUMENT = {
        'label': 'Documents',
        'rdf-type': 'nfo:Document',
        'icon-name': 'text-x-generic',
    }

    IMAGE = {
        'label': 'Pictures',
        'rdf-type': 'nfo:Image',
        'icon-name': 'image-x-generic',
    }

    VIDEO = {
        'label': 'Videos',
        'rdf-type': 'nmm:Video',
        'icon-name': 'video-x-generic',
    }


_type_mapping = {content_type.value['rdf-type']: content_type
                 for content_type in Types}

_namespace_mapping = {
    'http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#': 'nfo:',
    'http://www.tracker-project.org/temp/nmm#': 'nmm:'
}



def type_from_nepomuk_type_list(nepomuk_type_list):
    content_type = None
    for nepomuk_type in nepomuk_type_list:
        # Tracker gives us full URIs, but internally we use compact URIs.
        for ns, prefix in _namespace_mapping.items():
            nepomuk_type = nepomuk_type.replace(ns, prefix)

        new_content_type = _type_mapping.get(nepomuk_type)

        # Some files contain multiple content streams but don't need to be displayed
        # as such.
        if content_type == Types.VIDEO and new_content_type == Types.AUDIO:
            # It's implicit that videos contain an audio track and it would be
            # confusing if we showed each video file as video+audio.
            pass
        else:
            content_type = new_content_type
    return content_type
