# Tagcloud
#
# Copyright 2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gio, GLib, GObject

import enum
import logging


class Location(GObject.Object):
    def __init__(self, path, recursive=True, can_remove=False):
        super(Location, self).__init__()

        self.path = path
        self.recursive = recursive
        self.can_remove = can_remove

    def __str__(self):
        return "<Location {}>".format(self.path)

    def __repr__(self):
        return self.__str__()

    # These functions are needed in various places where we have set()s of
    # locations. They assume that all location objects for the same path
    # are equivalent. It should not be possible to get in situations where
    # a path is configured to be indexed twice with two different
    # configurations as this would make no sense anyway.

    def __hash__(self):
        return hash(self.path)

    def __eq__(self, other):
        return (self.path == other.path)

    def __lt__(self, other):
        return self.path < other.path

    def __gt__(self, other):
        return self.path > other.path

    def _set_indexing(self, indexing):
        self._indexing = indexing

    @GObject.Property(type=bool, default=False)
    def indexing(self):
        return _indexing


class RemovableDeviceAction(enum.Enum):
    # This must correspond with the enums in
    # data/uk.me.afuera.Tagcloud.RemovableDevice.gschema.xml.
    ASK = 0
    IGNORE = 1
    INDEX = 2


class RemovableDevice(Location):
    def __init__(self, mount):
        super(RemovableDevice, self).__init__(mount.get_root().get_path(), recursive=True, can_remove=False)

        self.mount = mount

        self._name = self.mount.get_name()

        self._uuid = self.mount.get_uuid()
        if self._uuid is None:
            volume = self.mount.get_volume()
            if volume:
                self._uuid = volume.get_uuid()

        if self._uuid:
            self.settings = Gio.Settings.new_with_path(
                'uk.me.afuera.Tagcloud.RemovableDevice',
                '/uk/me/afuera/Tagcloud/removable-devices/{}/'.format(self.uuid()))

            self.settings.connect('changed::action', lambda obj, key: self.set_action(RemovableDeviceAction(obj.get_enum(key))))
            self._action = RemovableDeviceAction(self.settings.get_enum('action'))
        else:
            logging.warn("No UUID for device {}, cannot save settings".format(self))
            self.settings = None
            self._action = RemovableDeviceAction.ASK

    def __str__(self):
        return "<RemovableDevice {}:{}>".format(self.name(), self.uuid())

    def name(self):
        return self._name

    def uuid(self):
        return self._uuid

    # The 'action' property controls whether we should be indexing the device,
    # ignoring it, or asking the user what to do.
    def get_action(self):
        return self._action

    def set_action(self, action):
        if isinstance(action, int):
            action = RemovableDeviceAction(action)
        if action != self._action:
            logging.debug("{}: action changed {} -> {}".format(self, action, self._action))
            self._action = action
            if self.settings is not None:
                self.settings.set_enum('action', action.value)
            GLib.idle_add(lambda: self.notify('action'))

    action = GObject.Property(type=int, default=RemovableDeviceAction.ASK.value,
                              getter=get_action, setter=set_action)


class LocationManager(GObject.Object):
    '''Keeps track of locations on disk that are being scanned for content.'''

    def __init__(self, tracker_domain, volume_monitor):
        super(LocationManager, self).__init__()

        self._tracker_domain = tracker_domain

        # These are the locations on the hard disk which we configure Tracker
        # to index.
        self._tracker_locations = []
        self._tracker_domain.connect('notify::configured-indexing-locations',
                                     lambda obj, pspec: self._update_tracker_locations())
        self._update_tracker_locations()

        # Then we have the removable devices, which are a bit more tricky. We
        # can't just configure Tracker to index the mountpoints as when the
        # device is unmounted Tracker will think all the data was deleted and
        # remove it from the store. We can't configure Tracker to always index
        # removable devices that it sees because this may lead to it scanning
        # large volumes when the user doesn't want it to. We have to track each
        # device and whether the user wants each one to be indexed, prompting
        # them if necessary when the device appears.
        self._volume_monitor = volume_monitor
        self._volume_monitor.connect('mount-added', self._on_mount_added)
        self._volume_monitor.connect('mount-removed', self._on_mount_removed)
        self._removable_devices = []
        for mount in self._volume_monitor.get_mounts():
            self._on_mount_added(self._volume_monitor, mount)

        self._indexing = {}

    @GObject.Property(GLib.Array)
    def locations(self):
        '''List of all locations that are being indexed.

        This is a list of Location instances.

        '''
        removable_locations = [device for device in self._removable_devices if device.action == RemovableDeviceAction.INDEX]
        return sorted(self._tracker_locations + removable_locations)

    @GObject.Property(GLib.Array)
    def removable_devices(self):
        '''List of all removable devices that are currently connected.

        Each one may or may not be indexed depending on the user's choice.

        '''
        return sorted(self._removable_devices)

    def _update_tracker_locations(self):
        self._tracker_locations = []
        for path, recursive in self._tracker_domain.configured_indexing_locations:
            if path.startswith('&'):
                # This can happen when the target directory doesn't exist.
                logging.warn("Ignoring unexpanded directory name {}".format(path))
            else:
                self._tracker_locations.append(Location(path, recursive, can_remove=True))

        self.notify('locations')

    def is_removable_device(self, mount):
        if mount.is_shadowed():
            logging.debug("location manager: ignoring shadowed mount {}".format(mount))
            return False

        drive = mount.get_drive()
        if drive is None:
            logging.debug("location manager: ignoring mount with no drive {}".format(mount))
            return False

        return drive.is_removable()

    def _on_mount_added(self, monitor, mount):
        logging.debug("Got new mount: {}".format(mount))
        if self.is_removable_device(mount):
            device = RemovableDevice(mount)

            logging.debug("Removable device added: {}".format(device))

            self._removable_devices.append(device)

            device.connect('notify::action', lambda obj, pspec: self.on_removable_device_action_changed(obj))
            self.on_removable_device_action_changed(device)

            self.notify('removable-devices')

    def _on_mount_removed(self, monitor, mount):
        to_remove = []
        for device in self._removable_devices:
            if device.mount == mount:
                to_remove.append(device)
        logging.debug("Removable device removed: {}".format(to_remove))
        while len(to_remove) > 0:
            self._removable_devices.remove(to_remove.pop())
        self.notify('locations')
        self.notify('removable_devices')

    def add_location(self, path):
        '''Add 'path' as a location to be indexed.

        This updates the configuration of the Tracker domain. Whether this
        affects the content that will appear in other applications depends
        on how the application was started (the --tracker-domain argument).

        '''
        self._tracker_domain.add_indexing_location(path, recursive=True)

    def remove_location(self, path):
        '''Remove 'path' from the locations that the Tracker domain indexes.'''

        self._tracker_domain.remove_indexing_location(path)

    def on_removable_device_action_changed(self, device):
        def idle_callback():
            logging.debug("Indexing of {} complete".format(device))

        action = device.get_action()
        if action == RemovableDeviceAction.INDEX:
            if device in self._indexing:
                logging.warn("{} is already being indexed!!")
            else:
                logging.debug("Indexing {}".format(device))
                self._indexing[device] = True
                self._tracker_domain.index_location_async(device.path, None, idle_callback)
                GLib.idle_add(lambda: self.notify('locations'))
        elif action == RemovableDeviceAction.IGNORE:
            logging.debug("No longer indexing {}".format(device))
            logging.warn("Unable to cancel ongoing indexing for {}, see https://gitlab.gnome.org/GNOME/tracker/issues/7".format(device))
            GLib.idle_add(lambda: self.notify('locations'))
        else:
            # The status could change to 'ASK', but the application itself will
            # never cause that to happen. Rather than handle this action here,
            # it's detected by the Application instance.
            pass
