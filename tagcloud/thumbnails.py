# Tagcloud
#
# Copyright 2017 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('GnomeDesktop', '3.0')
from gi.repository import Gio, GLib, GnomeDesktop

import logging
import queue
import threading
import warnings


G_PRIORITY_LOW = 300


class QueueEntry:
    def __init__(self, uri, mime_type, mtime, callback):
        self.uri = uri
        self.mime_type = mime_type
        self.mtime = mtime
        self.callback = callback

    def __str__(self):
        return self.uri


class _ThumbnailerThread(threading.Thread):
    '''Create thumbnails using the libgnomedesktop thumbnailer API

    The GnomeDesktop library provides a standard, sandboxed method of
    generating thumbnails from files. It's used by the Nautilis file manager
    and other programs too. It doesn't provide an async API at the time of
    writing, so it must be used from a separate thread to avoid blocking the
    app UI.

    '''
    def __init__(self):
        super(_ThumbnailerThread, self).__init__()
        self._queue = queue.Queue()
        self._factory = GnomeDesktop.DesktopThumbnailFactory.new(
            GnomeDesktop.DesktopThumbnailSize.NORMAL)


    def run(self):
        logging.debug("thumbnailer-thread started.")
        while True:
            entry = self._queue.get()
            logging.debug("thumbnailer-thread: Processing item %s" % entry)
            pixbuf = self._create_thumbnail_for_file_sync(
                self._factory, entry.uri, entry.mime_type, entry.mtime)
            # The callback will likely call into Gtk (perhaps triggered by
            # signal emissions), so it must be run from the main thread to
            # avoid races.
            GLib.idle_add(entry.callback, pixbuf)
            self._queue.task_done()

    def create_thumbnail_for_url(self, url, callback=None):
        '''Called from main thread.'''
        gfile = Gio.File.new_for_uri(url)
        def query_file_info_callback(obj, result):
            try:
                gfile_info = obj.query_info_finish(result)
                mime_type = gfile_info.get_attribute_string('standard::content-type')
                mtime = gfile_info.get_attribute_uint64('time::modified')
                if self._factory.can_thumbnail(url, mime_type, mtime):
                    self._queue.put(QueueEntry(url, mime_type, mtime, callback))
                else:
                    warnings.warn("No support for thumbnailing type %s" % mime_type)
            except GLib.Error as e:
                logging.warn("Error getting info for %s: %s" % (url, e))
        gfile.query_info_async('standard::content-type,time::modified',
                               Gio.FileQueryInfoFlags.NONE,
                               G_PRIORITY_LOW, None, query_file_info_callback)

    def _create_thumbnail_for_file_sync(self, factory, url, mime_type, mtime):
        pixbuf = None
        if factory.can_thumbnail(url, mime_type, mtime):
            pixbuf = factory.generate_thumbnail(url, mime_type)
            if pixbuf:
                logging.debug("thumbnailer-thread: Saving thumbnail for %s", url)
                factory.save_thumbnail(pixbuf, url, mtime)
            else:
                logging.debug("thumbnailer-thread: Saving failed thumbnail marker for %s", url)
                factory.create_failed_thumbnail(url, mtime)
        return pixbuf


_thread = None
def thumbnailer_thread():
    global _thread
    if not _thread:
        _thread = _ThumbnailerThread()
        _thread.setDaemon(True)
        _thread.start()
    return _thread
