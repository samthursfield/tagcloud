# Tagcloud
#
# Copyright 2017 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Tracker', '2.0')
from gi.repository import Gio, GLib, GObject, Gtk, Tracker

import os

TAGCLOUD_GRAPH_URN = 'graph://uk.me.afuera.Tagcloud'


class Actions:
    '''Performs edit actions on the database or filesystem.'''
    def __init__(self, db):
        self.db = db

    def _update_callback(self, obj, result):
        obj.update_finish(result)

    def add_tag(self, content_items, tag_label):
        tag_uri = Tracker.sparql_escape_uri('urn:tag:%s' % tag_label)

        tag = Tracker.Resource.new(tag_uri)
        tag.set_uri('rdf:type', 'nao:Tag')
        tag.set_string('nao:prefLabel', tag_label)

        updates = []

        # FIXME: it's a bit inefficient here to do a separate SPARQL update for
        # each resource; we should write the query ourself instead of using
        # TrackerResource. Maybe TrackerSparqlBuilder is best here??
        for resource_urn in [item.tracker_urn for item in content_items]:
            resource = Tracker.Resource.new(resource_urn)
            resource.add_relation('nao:hasTag', tag)
            updates.append(resource.print_sparql_update(None, TAGCLOUD_GRAPH_URN))

        # FIXME: this doesn't work because pygi doesn't support functions
        # returning GError.
        #
        #def callback(obj, result):
        #    obj.update_array_finish(result)
        #
        #self.db.update_array_async(updates, 0, None, callback)

        for update in updates:
            self.db.update_async(update, 0, None, self._update_callback)

    def delete_tags(self, tag_id_list):
        for tag_urn in tag_id_list:
            update_1 = ('DELETE { GRAPH <%s> { <%s> ?any_pred ?any_obj } } '
                        'WHERE { <%s> ?any_pred ?any_obj }' % (
                        TAGCLOUD_GRAPH_URN, tag_urn, tag_urn))
            update_2 = ('DELETE { GRAPH <%s> { ?r nao:hasTag <%s> } } '
                        'WHERE { ?r nao:hasTag <%s> }' % (
                        TAGCLOUD_GRAPH_URN, tag_urn, tag_urn))
            self.db.update_async(update_1, 0, None, self._update_callback)
            self.db.update_async(update_2, 0, None, self._update_callback)

    def trash_items(self, items):
        for item in items:
            Gio.File.new_for_path(os.path.join(item.path, item.file_name)).trash()
