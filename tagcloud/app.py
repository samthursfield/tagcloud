# Tagcloud
#
# Copyright 2017-2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Tracker', '2.0')
from gi.repository import Gdk, Gio, GLib, GObject, Gtk, Tracker

from gettext import gettext as _

import copy
import logging
import time

import tagcloud.contentlistmodel
import tagcloud.locationmanager
import tagcloud.notifications
import tagcloud.taglistmodel
import tagcloud.trackercontrol

import tagcloud.ui.aboutdialog
import tagcloud.ui.mainwindow
import tagcloud.ui.settingsdialog


class Application(Gtk.Application):
    def __init__(self, tracker_domain, volume_monitor, app_version, app_id_suffix=None):
        application_id = 'uk.me.afuera.Tagcloud'

        # This is allowed for testing purposes only.
        if app_id_suffix:
            application_id += app_id_suffix

        self.app_version = app_version

        Gtk.Application.__init__(self, application_id=application_id,
                                 flags=Gio.ApplicationFlags.FLAGS_NONE)

        self.tracker_domain = tracker_domain
        self.volume_monitor = volume_monitor

        self.settings = Gio.Settings.new('uk.me.afuera.Tagcloud')

        css_provider_file = Gio.File.new_for_uri(
            'resource:///uk/me/afuera/Tagcloud/tagcloud.css')
        css_provider = Gtk.CssProvider()
        css_provider.load_from_file(css_provider_file)
        screen = Gdk.Screen.get_default()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(
            screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        self._window = None
        self._settings_dialog = None

        self._db = None
        self._location_manager = None

        self._tracker_status_update_idle = None

    def _add_simple_action(self, name, args=None, callback=None):
        action = Gio.SimpleAction.new(name, args)
        if callback:
            action.connect('activate', lambda action, param: callback())
        Gtk.Application.add_action(self, action)

    def do_startup(self):
        self._add_simple_action('settings', callback=self.present_settings_dialog)
        self._add_simple_action('about', callback=self.present_about_dialog)
        Gtk.Application.do_startup(self)

    @GObject.Signal
    def database_ready(self):
        '''Signal emitted once the app has connected to the database.

        The application window is shown before the database connection is
        complete, in order to be responsive and avoid blocking without doing
        anything in case it takes a long time to connect.

        Automated tests need to wait until the connection is ready however,
        so this signal gives them something to wait for.

        '''
        pass

    def do_activate(self):
        if not self._window:
            # When using a private Tracker domain, the daemons will only run
            # when something owns the corresponding domain name on the bus.
            # That's why we only call this function here now that our
            # GApplication instance has got itself set up.
            self.setup_tracker_async(self.on_database_connected)

            self._notification_manager = tagcloud.notifications.Notifications()
            self._window = tagcloud.ui.mainwindow.MainWindow(self, self._notification_manager)

        self._window.present()

    def present_settings_dialog(self, timestamp=None):
        if not self._settings_dialog:
            if self._location_manager:
                self._settings_dialog = tagcloud.ui.settingsdialog.SettingsDialog(self, self._location_manager)
                self._settings_dialog.set_transient_for(self._window)
            else:
                logging.warn("Cannot show settings dialog: no locationmananager available.")
                return

        def on_settings_dialog_destroyed(widget):
            self._settings_dialog = None
        self._settings_dialog.connect('destroy', on_settings_dialog_destroyed)

        self._settings_dialog.present_with_time(timestamp or time.time())

    def present_about_dialog(self, timestamp=None):
        dialog = tagcloud.ui.aboutdialog.AboutDialog(self.app_version)
        dialog.set_transient_for(self._window)
        dialog.present_with_time(timestamp or time.time())

    def setup_tracker_async(self, callback):
        '''Does any necessary Tracker setup and connects to the Tracker database.

        The callback() function is called with a Tracker.SparqlConnection object
        once the connection is established.

        '''
        def connection_cb(data, result):
            connection = Tracker.SparqlConnection.get_finish(result)
            callback(connection)

        def start_cb():
            self.tracker_domain.connect('notify::indexing', lambda obj, pspec: self.on_tracker_status_changed())
            self.tracker_domain.connect('notify::status_message', lambda obj, pspec: self.on_tracker_status_changed())

            return self.tracker_domain.connection_async(connection_cb)

        self.tracker_domain.start_async(start_cb)

    def on_database_connected(self, db):
        self._db = db

        content = tagcloud.contentlistmodel.ContentListModel(self._db)
        tags = tagcloud.taglistmodel.TagListModel(self._db)
        actions = tagcloud.actions.Actions(self._db)
        self._location_manager = tagcloud.locationmanager.LocationManager(
            self.tracker_domain, self.volume_monitor)

        self._window.set_content_list_model(content)
        self._window.set_tag_list_model(tags)
        self._window.set_actions_controller(actions)
        self._window.set_location_manager(self._location_manager)

        self._location_manager.connect('notify::removable-devices', lambda obj, pspec: self.on_removable_devices_changed())
        self.on_removable_devices_changed()

        self.emit('database_ready')

    def tracker_domain_name(self):
        return self.tracker_domain.domain_name()

    def on_tracker_status_changed(self):
        def update_tracker_status():
            if self.tracker_domain.get_property('indexing'):
                self._window.set_indexing_status(True, message=self.tracker_domain.get_property('status_message'))
            else:
                self._window.set_indexing_status(False)
            self._tracker_status_update_idle = None

        if self._tracker_status_update_idle is None:
            self._tracker_status_update_idle = GLib.idle_add(update_tracker_status)

    def on_removable_devices_changed(self):
        ns = self._notification_manager.removable_device_notifications()
        removable_devices = copy.copy(self._location_manager.removable_devices)

        for n in ns:
            if n.device not in removable_devices:
                self._notification_manager.remove(n)
            else:
                removable_devices.remove(n.device)
        for device in removable_devices:
            if device.get_action() == tagcloud.locationmanager.RemovableDeviceAction.ASK:
                self._notification_manager.add_removable_device_notification(device)
