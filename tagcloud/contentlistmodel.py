# Tagcloud
#
# Copyright 2017-2018 Sam Thursfield <sam@afuera.me.uk>
#
# Tagcloud is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Tagcloud is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Tracker', '2.0')
from gi.repository import GdkPixbuf, Gio, GLib, GObject, Gtk, Tracker

import contextlib
import copy
import difflib
import logging
import os.path
import pathlib
import urllib.parse
import warnings

import tagcloud.content
import tagcloud.pagedmodel
import tagcloud.thumbnails
import tagcloud.taglistmodel


G_PRIORITY_LOW = 300


@contextlib.contextmanager
def ignore_cancellation():
    try:
        yield
    except GLib.Error as e:
        if e.matches(Gio.io_error_quark(), Gio.IOErrorEnum.CANCELLED):
            logging.debug("Operation was cancelled, ignoring the error.")
        else:
            raise


def query_item_metadata(db, callback, item_urn, property_map):
    query_parts = ['SELECT']
    for predicate, name in property_map.items():
        query_parts.append('%s as ?%s' % (predicate, name))
    query_parts += ['{ SELECT <%s> as ?r {} }' % item_urn]

    query = ' '.join(query_parts)

    def query_callback(obj, result):
        cursor = obj.query_finish(result)
        cursor.next(None)
        property_values = {}
        for i, name in enumerate(property_map.values()):
            property_values[name] = cursor.get_string(i)[0]
        callback(property_values)
    logging.debug("Running metadata query: %s", query)
    db.query_async(query, None, query_callback)


class ContentItem(GObject.Object):
    '''Data model for an individual content item (normally a local file).'''

    MAX_THUMBNAIL_WIDTH = 128  # pixels

    def __init__(self, db, tracker_id, tracker_urn, type_list, url, file_name,
                 title, tags_model):
        super(ContentItem, self).__init__()

        self.db = db
        self.tracker_id = tracker_id
        self.tracker_urn = tracker_urn

        self.url = url
        self.file_name = file_name
        self.title = title
        self.tags_model = tags_model

        scheme, netloc, path, params, query, fragment = urllib.parse.urlparse(url)
        if scheme == 'file':
            self.path = os.path.dirname(urllib.parse.unquote(path))
        else:
            self.path = None

        content_type = tagcloud.content.type_from_nepomuk_type_list(type_list)
        if not content_type:
            raise RuntimeError("No type found for %s (RDF types are %s)" % (title, type_list))
        self.content_type = content_type.value

        self.metadata = {
            'filename': self.file_name,
            'path': self.path,
            'title': self.title,
        }
        self.has_full_metadata = False

        self._thumbnail_pixbuf = None

    # With this two methods, we ensure that two contentitem instances that
    # represent the same Tracker resource are considered equal. This is used
    # when updating the stored list of items with results from a new query.
    def __eq__(self, other):
        return self.tracker_urn == other.tracker_urn

    def __hash__(self):
        return hash(self.tracker_urn)

    @GObject.Signal
    def metadata_changed(self):
        pass

    @GObject.Property(type=GdkPixbuf.Pixbuf)
    def thumbnail(self):
        '''This property is updated by find_or_create_thumbnail_async().'''
        return self._thumbnail_pixbuf

    def thumbnail_height(self):
        return 64

    def query_full_metadata_async(self):
        property_query_map = {
            'nfo:fileSize(?r)': 'size',
            'nie:contentCreated(?r)': 'created',
            'nfo:fileLastModified(?r)': 'modified',
        }

        if self.content_type == tagcloud.content.Types.AUDIO:
            property_query_map.update({
                'nmm:artistName(nmm:performer(?r))': 'artist',
                # FIXME: a track can actually appear on multiple albums, but
                # Tracker doesn't get this yet.
                'nie:title(nmm:musicAlbum(?r))': 'album-title',
                'nmm:trackNumber(?r)': 'album-track',
                'nfo:duration(?r)': 'duration',
            })

        def query_metadata_callback(property_value_map):
            self.has_full_metadata = True
            self.metadata.update(property_value_map)
            self.emit('metadata_changed')

        query_item_metadata(self.db, query_metadata_callback, self.tracker_urn,
                            property_query_map)

    def find_or_create_thumbnail_async(self):
        gfile = Gio.File.new_for_uri(self.url)
        def create_thumbnail_callback(pixbuf):
            if pixbuf:
                self._thumbnail_pixbuf = pixbuf
                self.notify('thumbnail')
        def query_file_info_callback(obj, result):
            try:
                gfile_info = obj.query_info_finish(result)
                thumbnail_path = gfile_info.get_attribute_byte_string('thumbnail::path')
                if thumbnail_path:
                    self._thumbnail_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
                        thumbnail_path, self.MAX_THUMBNAIL_WIDTH, self.thumbnail_height())
                    self.notify('thumbnail')
                else:
                    thumbnailer = tagcloud.thumbnails.thumbnailer_thread()
                    thumbnailer.create_thumbnail_for_url(self.url, create_thumbnail_callback)
            except GLib.Error as e:
                #logging.warn("Error getting thumbnail for %s: %s" % (self.url, e))
                pass
        gfile.query_info_async('thumbnail::path', Gio.FileQueryInfoFlags.NONE,
                               G_PRIORITY_LOW, None, query_file_info_callback)


def count_resources_async(db, cancellable, callback, filter_pattern=''):
    query = ' '.join([
        'SELECT COUNT(?r) {',
            '?r a nie:InformationElement',
                '; rdf:type ?type '
                '; nie:url ?url .',
            filter_pattern,
        '}'])

    def query_next_callback(cursor, result):
        with ignore_cancellation():
            cursor.next_finish(result)
            callback(cursor.get_integer(0))

    def query_callback(obj, result):
        with ignore_cancellation():
            cursor = obj.query_finish(result)
            cursor.next_async(cancellable, query_next_callback)

    logging.debug("Running count query: %s", query)
    db.query_async(query, cancellable, query_callback)


def query_resources_async(db, cancellable, callback, filter_pattern='', offset=0,
                          limit=100):
    query = ' '.join([
        'SELECT',
            'tracker:id(?r)',
            '?r',
            'GROUP_CONCAT(?type, ",")',
            '?url',
            '?file_name',
            'COALESCE(?music_title, ?nie_title, ?file_name) AS ?title',
         '{',
            '?r a nie:InformationElement',
                '; nie:url ?url',
                '; nfo:fileName ?file_name',
                '; rdf:type ?type .',
            'OPTIONAL { ?r nie:title ?nie_title . }',
            'OPTIONAL { ?r nmm:performer [ nmm:artistName ?artist_name ] . }',
            'BIND (fn:concat(?artist_name, " - ", ?nie_title) AS ?music_title)',
            filter_pattern,
         '}',
         'GROUP BY ?r',
         'ORDER BY ?title',
         'OFFSET %i LIMIT %i' % (offset, limit)])

    logging.debug("Query: %s", query)

    # This is spaghetti code because each call to tracker_sparql_cursor_next()
    # can block for a while in the bowels of SQLite, so we need to jump back to
    # the GLib main loop as often as possible to avoid UI lockups.

    def query_next_callback(cursor, result, items):
        with ignore_cancellation():
            if cursor.next_finish(result):
                tracker_id = int(cursor.get_string(0)[0])
                tracker_urn = cursor.get_string(1)[0]
                tags_model = tagcloud.taglistmodel.TagsListModelForResource(cursor.get_connection(), tracker_id, tracker_urn)
                item = ContentItem(
                    db=cursor.get_connection(),
                    tracker_id=tracker_id,
                    tracker_urn=tracker_urn,
                    type_list=cursor.get_string(2)[0].split(','),
                    url=cursor.get_string(3)[0],
                    file_name=cursor.get_string(4)[0],
                    title=cursor.get_string(5)[0],
                    tags_model=tags_model,
                )

                items.append(item)

                cursor.next_async(cancellable, query_next_callback, items)
            else:
                callback(items)

    def query_callback(conn, result):
        with ignore_cancellation():
            cursor = conn.query_finish(result)
            items = []
            cursor.next_async(cancellable, query_next_callback, items)

    db.query_async(query, cancellable, query_callback)


class _ContentView():
    '''Represents a particular view of the available content.

    This holds the parameters that define a query, and (once the query has
    completed) also the results of that query.

    '''
    def __init__(self, page, page_size, content_type_filter,
                 location_filter, tag_filter, text_filter):
        self.page = page
        self.page_size = page_size
        self.content_type_filter = content_type_filter
        self.location_filter = location_filter
        self.tag_filter = tag_filter
        self.text_filter = text_filter

        self.page_count = 0
        self.total_items = 0
        self.items = []

    def copy(self):
        dup = copy.copy(self)
        dup.page_count = 0
        dup.total_items = 0
        dup.items = []
        return dup


class _ContentQuery(GObject.Object):
    '''Represents a change to the view that has been requested by the model.

    The query is processed asynchronously, and the contentlistmodel will signal
    the appropriate changes when the query has completed.

    '''
    def __init__(self, model, callback, view):
        super(_ContentQuery, self).__init__()
        self._cancellable = Gio.Cancellable.new()
        self._source_id = None

        self.db = model.db
        self._view = view
        self._callback = callback

        # GTask is unusable from bindings :-(
        #self._task = Gio.Task.new(self, self._cancellable, callback)

    def start(self):
        # FIXME: if we could use GTask, it would be simple to run all our
        # queries in a thread instead of weaving it all through GLib main
        # loop callbacks ...
        self._source_id = GLib.idle_add(self._run)

    def cancel(self):
        if self._source_id:
            GLib.source_remove(self._source_id)
        self._cancellable.cancel()

    def _run(self):
        self._counted = False
        self._queried = False

        def count_items_callback(total_items):
            self._view.total_items = total_items
            self._view.page_count = max(total_items / self._view.page_size, 1)

            if self._view.page > self._view.page_count:
                self._view.page = self._view.page_count

            self._counted = True
            if (self._counted and self._queried):
                self._callback(self._view)

        def query_resources_callback(items):
            self._view.items = items

            self._queried = True
            if (self._counted and self._queried):
                self._callback(self._view)

        if len(self._view.location_filter) == 0:
            # The location filter is not really optional as the Tracker database
            # may contain content from all kinds of directories that are no longer
            # configured to be indexed but that hasn't been deleted from the store.
            logging.debug("No locations specified, simulating an empty query result.")
            count_items_callback(0)
            query_resources_callback([])
        else:
            location_filter_pattern = ''.join([' FILTER (',
                ' || '.join([
                    'fn:starts-with(nie:url(?r), "%s")' % pathlib.Path(location.path).as_uri()
                    for location in self._view.location_filter]),
                ') '])

            content_types = ','.join(content_type.value['rdf-type']
                                    for content_type in self._view.content_type_filter)
            content_type_filter_pattern = 'FILTER (?type IN (%s))' % content_types

            tag_filter_pattern = ''.join([
                '?r nao:hasTag <%s> . ' % tag_id
                for tag_id in self._view.tag_filter])

            if len(self._view.text_filter) > 0:
                text_filter_pattern = ' ?r fts:match "%s" . ' % self._view.text_filter
            else:
                text_filter_pattern = ''

            filter_pattern = (content_type_filter_pattern + tag_filter_pattern +
                              location_filter_pattern + text_filter_pattern)

            count_resources_async(self.db, self._cancellable, count_items_callback, filter_pattern)

            offset = (self._view.page - 1) * self._view.page_size
            query_resources_async(self.db, self._cancellable, query_resources_callback,
                                    filter_pattern, offset=offset, limit=self._view.page_size)

        self._source_id = None


class ContentListModel(GObject.Object, Gio.ListModel):
    '''Holds current filtered list of resources.'''
    def __init__(self, db, page_size=100):
        super(ContentListModel, self).__init__()

        self._view = _ContentView(
            page=1,
            page_size=page_size,
            content_type_filter=tagcloud.content.Types,
            location_filter=[],
            tag_filter=[],
            text_filter='',
        )

        self.db = db

        notify_types = [t.value['rdf-type'] for t in tagcloud.content.Types]
        # FIXME: it seems we shouldn't need NOTIFY_UNEXTRACTED... but the tests
        # demonstrate that we do.
        self.file_notifier = Tracker.Notifier.new(
            notify_types, Tracker.NotifierFlags.NOTIFY_UNEXTRACTED | Tracker.NotifierFlags.QUERY_URN)
        self.file_notifier.connect('events', self._on_file_notifier_event)

        self._populate_task = None
        self._populate(self._view)

    def _pending_view(self):
        '''Returns the current or pending ContentView.

        This should not be modified. If you want to create a new view, use
        _new_view().

        '''
        if self._populate_task:
            return self._populate_task._view
        else:
            return self._view

    def _new_view(self):
        '''Returns a new ContentView instance.

        The view parameters will be set to those of the current view, or the
        pending new view if there is already an outstanding change request.

        '''
        return self._pending_view().copy()

    def _populate(self, view):
        def populate_callback(view):
            # In order for GtkListBox to update the display without artefacts,
            # we need to emit items-changed correctly. This is tricky because
            # we leave filtering and ordering to the database, and the 'items'
            # list we get here is all new ContentItem instances even though
            # some of them may represent the same data as instances in the
            # existing self.items list.
            # We solve this by using a diff algorithm on the list and updating
            # self.items accordingly, emitting the items-changed signal as we
            # go.
            diff = difflib.SequenceMatcher(isjunk=None, a=self._view.items, b=view.items)
            offset = 0
            new_items = []
            for op, i1, i2, j1, j2 in diff.get_opcodes():
                if op == 'replace':
                    self._view.items[i1+offset:i2+offset] = view.items[j1:j2]
                    #print("rep: items-changed: {}, {}, {}".format(i1+offset, i2-i1, j2-j1))
                    self.items_changed(position=i1+offset, removed=i2-i1, added=j2-j1)
                    offset += (j2-j1) - (i2-i1)
                    new_items.extend(view.items[j1:j2])
                elif op == 'delete':
                    #print("del: items-changed: {}, {}, {}".format(i1+offset, i2-i1, 0))
                    self._view.items[i1+offset:i2+offset] = []
                    self.items_changed(position=i1+offset, removed=i2-i1, added=0)
                    offset -= (i2-i1)
                elif op == 'insert':
                    #print("add: items-changed: {}, {}, {}".format(i1+offset, 0, j2-j1))
                    self._view.items[i1+offset:i2+offset] = view.items[j1:j2]
                    self.items_changed(position=i1+offset, removed=0, added=j2-j1)
                    offset += (j2-j1)
                    new_items.extend(view.items[j1:j2])
                elif op == 'equal':
                    pass

            for item in new_items:
                GLib.idle_add(item.find_or_create_thumbnail_async)

            if view.page_count != self._view.page_count:
                self._view.page_count = view.page_count
                self.notify('page_count')

            if view.page != self._view.page:
                self._view.page = view.page
                self.notify('current_page')

            self._view.page_size = view.page_size
            self._view.content_type_filter = view.content_type_filter
            self._view.location_filter = view.location_filter
            self._view.tag_filter = view.tag_filter
            self._view.text_filter = view.text_filter

            self._populate_task = None

        if self._populate_task:
            self._populate_task.cancel()
        self._populate_task = _ContentQuery(self, populate_callback, view)
        self._populate_task.start()

    def _on_file_notifier_event(self, notifier, events):
        logging.debug("Got {} events from file notifier".format(len(events)))

        for event in events:
            event_type = event.get_event_type()
            file_urn = event.get_urn()

            if event_type in [Tracker.NotifierEventType.CREATE,
                              Tracker.NotifierEventType.UPDATE]:
                self._populate(self._new_view())
            elif event_type == Tracker.NotifierEventType.DELETE:
                self._populate(self._new_view())

    def get_current_page(self):
        return self._view.page

    def set_current_page(self, value):
        if value < 1 or value > self._view.page_count:
            raise ValueError("Page %i outside range [%i, %i]" % (value, 1, self._view.page_count))

        if value != self._pending_view().page:
            new_view = self._new_view()
            new_view.page = value
            self._populate(new_view)

    current_page = GObject.Property(type=int, minimum=1, default=1,
                                    getter=get_current_page,
                                    setter=set_current_page)
    def get_page_count(self):
        return self._view.page_count

    page_count = GObject.Property(type=int, getter=get_page_count)

    def do_get_n_items(self):
        return len(self._view.items)

    def do_get_item(self, position):
        item = self._view.items[position]
        return item

    def set_content_type_filter(self, content_types):
        new_view = self._new_view()
        new_view.content_type_filter = content_types
        self._populate(new_view)

    def set_location_filter(self, locations):
        new_view = self._new_view()
        new_view.location_filter = locations
        self._populate(new_view)

    def set_tag_filter(self, tag_ids):
        new_view = self._new_view()
        new_view.tag_filter = tag_ids
        self._populate(new_view)

    def set_text_filter(self, text):
        old_text_filter = self._pending_view().text_filter
        if len(text) > 2:
            new_text_filter = text
        else:
            new_text_filter = ''
        if old_text_filter != new_text_filter:
            new_view = self._new_view()
            new_view.text_filter = new_text_filter
            self._populate(new_view)
